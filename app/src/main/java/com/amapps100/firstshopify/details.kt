package com.amapps100.firstshopify

import android.animation.LayoutTransition
import android.app.ActionBar
import android.content.Context
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import kotlinx.android.synthetic.main.activity_details.*
import kotlin.collections.ArrayList
import android.renderscript.Allocation
import android.renderscript.Element

import android.renderscript.ScriptIntrinsicBlur

import android.renderscript.RenderScript
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.marginBottom
import androidx.core.view.marginTop
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.WindowCompat
import androidx.core.view.marginRight
import com.amapps100.firstshopify.CustomViewPager.ViewPagerInterface
import com.amapps100.firstshopify.adapters.ProductsAdapter
import com.parse.*
import com.shashank.sony.fancytoastlib.FancyToast
import java.text.SimpleDateFormat
import java.util.*


class details : AppCompatActivity(), ViewPagerInterface {

    var isFullscreen=false
    val PHONE_REQUEST_CODE=56421
    var objId:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContentView(R.layout.activity_details)

        objId=intent.getStringExtra("objectId")
        if(objId!=null){
            Parse.initialize(
                Parse.Configuration.Builder(this)
                    .applicationId(getString(R.string.back4app_app_id))
                    .clientKey(getString(R.string.back4app_client_key))
                    .server(getString(R.string.back4app_server_url))
                    .build())
            val installation = ParseInstallation.getCurrentInstallation()
            installation.saveInBackground()
            getSpecificObj(objId!!)
        }else{
            val name= ProductsAdapter.customProd.name
            val image_1=ProductsAdapter.customProd.image
            val image_2:Bitmap?=ProductsAdapter.customProd.image_2
            val image_3:Bitmap?=ProductsAdapter.customProd.image_3
            val image_4:Bitmap?=ProductsAdapter.customProd.image_4
            val desc=ProductsAdapter.customProd.desc
            val type=ProductsAdapter.customProd.type
            val state=ProductsAdapter.customProd.state
            val city=ProductsAdapter.customProd.city
            val location=ProductsAdapter.customProd.location
            val comm=ProductsAdapter.customProd.comm
            val comm_wa=ProductsAdapter.customProd.comm_wa
            val comm_tg=ProductsAdapter.customProd.comm_tg
            val pinned=ProductsAdapter.customProd.pinned
            val price=ProductsAdapter.customProd.price
            val objectId=ProductsAdapter.customProd.objectId
            val views=ProductsAdapter.customProd.views
            val username=ProductsAdapter.customProd.userName
            val pending=ProductsAdapter.customProd.pending
            val endDate=ProductsAdapter.customProd.endDate
            val date=ProductsAdapter.customProd.date

            castItem(
                productsData(name,image_1,image_2,image_3,image_4,desc,type,state,city,location,comm,comm_wa,comm_tg,
            pinned,price,objectId,views,username,pending,endDate,date)
            )
        }


        btn_backward.setOnClickListener {
            details_viewpager.setCurrentItem(details_viewpager.currentItem-1)
        }
        btn_forward.setOnClickListener {
            details_viewpager.setCurrentItem(details_viewpager.currentItem+1)
        }
        

        btn_full.setOnClickListener{
            if(isFullscreen){
                exitFullScreen()
            }else{
                enterFullScreen()
            }
        }



    }
    private fun castItem(item:productsData){
        if(item.comm_tg){
            btn_tg.visibility= View.VISIBLE
            btn_tg.setOnClickListener {
                Log.d("2323","openning: https://t.me/+963${item.comm} ")
                openIntent("https://t.me/+963${item.comm}")
            }
        }
        if(item.comm_wa){
            btn_wa.visibility= View.VISIBLE
            btn_wa.setOnClickListener {
                openIntent("https://api.whatsapp.com/send?phone=963${item.comm}&text=مرحباً رأيت إعلانك ${item.name} على سوق الجمعة هل يمكنك إخباري بالمزيد من التفاصيل")
            }
        }
        btn_call.setOnClickListener {
            getphonePermession(item.comm)
        }
        name_details.text=item.name
        desc_details.text=item.desc
        desc_details.append("\n \n")
        desc_details.movementMethod=ScrollingMovementMethod()
        type_details.text=item.type
        state_details.text=item.state
        city_details.text=item.city
        comm_details.text=item.comm
        location_details.text=item.location
        price_details.text=item.price
        user_details.text=item.userName
        val image_1= item.image
        val image_2= item.image_2
        val image_3= item.image_3
        val image_4= item.image_4
        val array=ArrayList<Bitmap>()
        array.add(image_1)
        imageView_backback.setImageBitmap(applyBlur(this,image_1,4f))
        if(image_2 != null){
            array.add(image_2)
        }
        if(image_3 != null){
            array.add(image_3)
        }
        if(image_4 != null){
            array.add(image_4)
        }
        details_viewpager.adapter=CustomViewPager(this,array,this)
        tabLayout_details.setupWithViewPager(details_viewpager)
    }

    private fun getSpecificObj(objId: String) {
        cont_lottie_loading_details.visibility=View.VISIBLE
        val query= ParseQuery.getQuery<ParseObject>("products")
        query.getInBackground(objId
        ) { i, e ->
            if(e==null){
                val name = i.get("name").toString()
                val desc = i.get("desc").toString()
                val state = i.get("new_used").toString()
                val type1 = i.get("type").toString()
                val comm = i.get("comm").toString()
                val comm_tg=i.getBoolean("comm_tg")
                val comm_wa=i.getBoolean("comm_wa")
                val location = i.get("location").toString()
                val city1 = i.get("city").toString()
                val price = i.get("price").toString()
                val views = i.get("views").toString()
                val userName = i.get("username").toString()
                val Date = i.get("mdate").toString()
                var endDate = Date
                val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                val c = Calendar.getInstance()
                c.time = sdf.parse(endDate)
                c.add(Calendar.DATE, 10) // number of days to add
                endDate = sdf.format(c.time)
                val pending = i.getBoolean("pending")
                val objectId=i.objectId
                val pinned : Boolean=i.getBoolean("pinned")
                val image_1 = i.getParseFile("image1")
                val image_2 = i.getParseFile("image2")
                val image_3 = i.getParseFile("image3")
                val image_4 = i.getParseFile("image4")
                val bitmap = BitmapFactory.decodeStream(image_1!!.dataStream)
                var bitmap_2 : Bitmap?=null
                if(image_2 != null){
                    bitmap_2 = BitmapFactory.decodeStream(image_2.dataStream)
                }
                var bitmap_3 : Bitmap?=null
                if(image_3 != null){
                    bitmap_3 = BitmapFactory.decodeStream(image_3.dataStream)
                }
                var bitmap_4 : Bitmap?=null
                if(image_4 != null){
                    bitmap_4 = BitmapFactory.decodeStream(image_4.dataStream)
                }
                val obj = productsData(name, bitmap,bitmap_2,bitmap_3,bitmap_4, desc,type1,state,city1,location,comm,comm_wa,comm_tg,pinned,price,objectId,views,userName,pending,endDate,Date)
                castItem(obj)
                cont_lottie_loading_details.visibility=View.GONE
            }else{
                toast(this,"حدث خطأ ما, يرجى المعاودة لاحقاً",FancyToast.ERROR)

                Log.d("232",e.message.toString())
                finishAffinity()
            }
        }

    }

    private fun getphonePermession(comm:String){
        if(ContextCompat.checkSelfPermission(this,android.Manifest.permission.CALL_PHONE)
            != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.CALL_PHONE),PHONE_REQUEST_CODE)
        }else{
            val intent = Intent(Intent.ACTION_CALL)
            intent.data = Uri.parse("tel:$comm")
            startActivity(intent)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == PHONE_REQUEST_CODE && grantResults.isNotEmpty()){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                getphonePermession(ProductsAdapter.customProd.comm)
            }else{
                toast(this,"الرجاء قبول إذن إجراء المكالمة للإكمال",FancyToast.WARNING)

            }
        }
    }
    private fun toast(c:Context,message:String,type:Int){
        FancyToast.makeText(c,message,FancyToast.LENGTH_SHORT,type,false).show()
    }
    private fun openIntent(url:String){
        val uri=Uri.parse(url)
        val i=Intent(Intent.ACTION_VIEW)
        i.data = uri
        startActivity(i)
    }
    private fun enterFullScreen() {
        (co3 as ViewGroup).layoutTransition.enableTransitionType(LayoutTransition.CHANGING)
        co3.layoutParams=RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT)
        val lm=cardview1.layoutParams as ConstraintLayout.LayoutParams
        lm.setMargins(1,cardview1.marginTop,1,cardview1.marginBottom)
        cardview1.layoutParams=lm
        btn_full.setImageResource(R.drawable.ic_baseline_fullscreen_exit_24)
        isFullscreen=true
    }

    private fun exitFullScreen() {
        (co3 as ViewGroup).layoutTransition.enableTransitionType(LayoutTransition.CHANGING)
        co3.layoutParams=RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT)
        val lm=cardview1.layoutParams as ConstraintLayout.LayoutParams
        lm.setMargins(55,cardview1.marginTop,55,cardview1.marginBottom)
        cardview1.layoutParams=lm
        btn_full.setImageResource(R.drawable.ic_baseline_fullscreen_24)
        val p=co3.layoutParams as ViewGroup.MarginLayoutParams
        p.setMargins(0,120,0,0)
        co3.requestLayout()
        isFullscreen=false
    }

    fun applyBlur(pContext: Context?, pSrcBitmap: Bitmap?, pBlurRadius: Float): Bitmap? {
        return if (pSrcBitmap != null) {
            val copyBitmap = pSrcBitmap.copy(Bitmap.Config.ARGB_8888, true)
            val outputBitmap = Bitmap.createBitmap(copyBitmap)
            val renderScript = RenderScript.create(pContext)
            val scriptIntrinsicBlur =
                ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript))
            val allocationIn = Allocation.createFromBitmap(renderScript, pSrcBitmap)
            val allocationOut = Allocation.createFromBitmap(renderScript, outputBitmap)
            scriptIntrinsicBlur.setRadius(pBlurRadius)
            scriptIntrinsicBlur.setInput(allocationIn)
            scriptIntrinsicBlur.forEach(allocationOut)
            allocationOut.copyTo(outputBitmap)
            outputBitmap
        } else {
            null
        }
    }

    override fun onBackPressed() {
        if(!isFullscreen){
            if(objId!=null){
                finish()
                startActivity(Intent(this,Products::class.java))
            }else{
            super.onBackPressed()}
            overridePendingTransition(R.anim.fade_in_fast,R.anim.fade_out_fast)
        }else{exitFullScreen()}
    }

    override fun fullScreenFuns() {
        if(isFullscreen){
            exitFullScreen()
        }else{
            enterFullScreen()
        }
    }
}