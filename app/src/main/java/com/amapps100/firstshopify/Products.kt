package com.amapps100.firstshopify

import android.app.Dialog
import android.content.*
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import com.marcoscg.dialogsheet.DialogSheet2
import com.parse.*
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.android.synthetic.main.activity_products.*
import kotlinx.android.synthetic.main.my_menu_logged.*
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList


import android.view.ViewGroup

import android.graphics.drawable.InsetDrawable

import android.graphics.drawable.ColorDrawable
import androidx.appcompat.widget.AppCompatButton
import com.airbnb.lottie.LottieAnimationView
import me.grantland.widget.AutofitTextView
import java.lang.Exception
import android.net.ConnectivityManager

import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.lottie.LottieDrawable
import com.amapps100.firstshopify.adapters.ProductsAdapter
import java.text.SimpleDateFormat
import android.os.AsyncTask
import android.os.SystemClock
import android.widget.*
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.AppCompatImageButton
import kotlinx.coroutines.delay
import kotlin.time.Duration


class Products : AppCompatActivity() {



    var typeFilterChecked : Boolean=false
    var cityFilterChecked : Boolean=false

    var numberOfReq=0
    var citySelected:String?=null
    var firstLaunch=true

    var checkedId:CheckedTextView? = null

    companion object{

        var mainProductList=ArrayList<productsData>()
        val allproductLisg=ArrayList<productsData>()
        var needLocalRefreshing=false
        val searchProductList=ArrayList<searchProdData>()
        var currentUser :String?=null
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.splashScreen)
        setContentView(R.layout.activity_products)
//        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        Parse.initialize(
            Parse.Configuration.Builder(this)
                .applicationId(getString(R.string.back4app_app_id))
                .clientKey(getString(R.string.back4app_client_key))
                .server(getString(R.string.back4app_server_url))
                .build())
        val installation = ParseInstallation.getCurrentInstallation()

        installation.put("GCMSenderId", "874668082358")
        installation.put("deviceModel",android.os.Build.MODEL)
        installation.put("api",android.os.Build.VERSION.SDK_INT.toString())

        val currentTempUser=ParseUser.getCurrentUser()
        if(currentTempUser != null){
            if(currentUser==null){
                val userName=currentTempUser.username
                currentUser=userName
                fancyToastmaker("أهلاً ${userName}",FancyToast.DEFAULT)
            }
        }
        if(currentUser!=null){
            installation.put("username", currentUser.toString())
        }else{
            installation.put("username","")
        }
        installation.saveInBackground()





        startService(Intent(this,NotyReciver::class.java))
        checkforfirstLaunch()
        dropdown_city_products.setText("الكل")
        val productsCity=resources.getStringArray(R.array.cityCustom)
        val arrayAdapter_city= ArrayAdapter(applicationContext,R.layout.dropdown_item,productsCity)
        dropdown_city_products.setAdapter(arrayAdapter_city)
        filterFunc()
        focusFix()
        if(currentUser==null){
            btn_sec.text="تسجيل دخول"
            btn_third.text="إنشاء حساب"
        }
        dropdown_city_products.onFocusChangeListener = object:View.OnFocusChangeListener{
            override fun onFocusChange(p0: View?, p1: Boolean) {
                if(p1){
                    closeKeyboard()

                }
            }
        }

        edt_search.setOnFocusChangeListener(object:View.OnFocusChangeListener{
            override fun onFocusChange(p0: View?, p1: Boolean) {
                if(p1){
                    if(!isNetworkConnected()){
                        oneButtonDialog("أنت غير متصل بالأنترنت\uD83D\uDE13 ","يرجى التحقق من اتصالك بالشبكة","حسناً",true,R.raw.nonet111,true)
                        closeKeyboard()
                        edt_search.clearFocus()
                    }
                }
            }
        })


        btn_openMenu.setOnClickListener{
            if(cont_menu.isVisible){
                closeMenu()
            }else{
                openMenu()
            }
        }

        cont_menu.setOnClickListener {
            closeMenu()
        }

        btn_telegram.setOnClickListener {
            val uri=Uri.parse("https://t.me/amapps100")
            val i=Intent(Intent.ACTION_VIEW)
            i.data = uri
//            i.putExtra(Intent.EXTRA_TEXT,)
            closeMenu()
            startActivity(i)
        }

//        btn_whatsapp.setOnClickListener {
//            val uri=Uri.parse("https://api.whatsapp.com/send?phone=PHONE_NUMBER")
//            val i=Intent(Intent.ACTION_VIEW)
//            i.data = uri
//            closeMenu()
//            startActivity(i)
//        }

        dropdown_city_products.setOnFocusChangeListener { p0, p1 ->
            if (p1) {
                if (!isNetworkConnected()) {
                    Log.d(p1.toString(), "112")
                    oneButtonDialog("أنت غير متصل بالأنترنت\uD83D\uDE13 ","يرجى التحقق من اتصالك بالشبكة","حسناً",true,R.raw.nonet111,true)
                    p0!!.clearFocus()
                }
            }
        }

        btn_first.setOnClickListener {
            if(isNetworkConnected()){
            if(currentUser!=null){
                var numberOfAds=0
                var customNumberOfAds=0
                for(i in allproductLisg){
                    if(i.userName== currentUser){
                        numberOfAds+=1
                    }
                }
                for(j in MyAds.lastUpdatedList){
                    if(j.userName== currentUser){
                        customNumberOfAds+=1
                    }
                }
                if(numberOfAds>=5 || customNumberOfAds>=5){
//                cant add more than 5 ads please delete some
                    adDialogeWarning()
                }else{
                    closeMenu()
                    startActivity(Intent(this,MainActivity::class.java))
//                    finish()
                }
            }else{
                dialogeCreatAccountOrLogin()
            }
            }else{
                oneButtonDialog("أنت غير متصل بالأنترنت\uD83D\uDE13 ","يرجى التحقق من اتصالك بالشبكة","حسناً",true,R.raw.nonet111,true)
            }
        }
        btn_sec.setOnClickListener {
            if(isNetworkConnected()){
            if(btn_sec.text.toString() == "حسابي"){
                closeMenu()
                startActivity(Intent(this,MyAds::class.java))

//                finish()
            }else{
                closeMenu()
                startActivity(Intent(this,Login::class.java))
            }
            }else{
                oneButtonDialog("أنت غير متصل بالأنترنت\uD83D\uDE13 ","يرجى التحقق من اتصالك بالشبكة","حسناً",true,R.raw.nonet111,true)
            }
        }

        btn_third.setOnClickListener {
            if(isNetworkConnected()){
            if(btn_third.text=="تسجيل الخروج"){
                currentUser=null
                ParseUser.logOut()
                installation.put("username","")
                installation.saveInBackground()
                fancyToastmaker("تم تسجيل الخروج",FancyToast.DEFAULT)
                closeMenu()
                btn_sec.text="تسجيل دخول"
                btn_third.text="إنشاء حساب"
                openMenu()
            }else{
                closeMenu()
                startActivity(Intent(this,Signup::class.java))
            }
            }else{
                oneButtonDialog("أنت غير متصل بالأنترنت\uD83D\uDE13 ","يرجى التحقق من اتصالك بالشبكة","حسناً",true,R.raw.nonet111,true)
            }
        }
        btn_cancleSearch.setOnClickListener {
            if(!edt_search.text.isBlank()){
                edt_search.setText("")
                closeKeyboard()
                if(edt_search.isFocused){
                    edt_search.clearFocus()
                }
            }
        }
        edt_search.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(!p0.toString().isBlank()){
                    btn_cancleSearch.setImageResource(R.drawable.ic_baseline_close_24)
                }else{
                    btn_cancleSearch.setImageResource(R.drawable.ic_baseline_search_24)
                }

                if(isNetworkConnected()){
                    if(p0.toString().isNotEmpty()){
                        val result=ArrayList<productsData>()
                        for(i in searchProductList){
                            if(i.name.contains(convertToEnglish(p0.toString())) ||
                                i.desc.contains(convertToEnglish(p0.toString()))||
                                i.state.contains(p0.toString())){
                                for(j in mainProductList){
                                    if(j.objectId==i.objId){
                                        result.add(j)
                                    }
                                }
                            }
                        }
                        castToRecycler(orderByPinned(result))

                    }else{
                        castToRecycler(mainProductList)

                    }
                }else{
                    oneButtonDialog("أنت غير متصل بالأنترنت\uD83D\uDE13 ","يرجى التحقق من اتصالك بالشبكة","حسناً",true,R.raw.nonet111,true)
                }
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })
    }
    private fun checkforfirstLaunch() {
        val sp=getSharedPreferences("db", MODE_PRIVATE)
        val isFirstlaunch=sp.getBoolean("isFirst",true)
        if(isFirstlaunch){
            showFirstLaunch()
            sp.edit().putBoolean("isFirst",false).apply()
        }else{
            getProductProtocol()
        }
        val isInfoReaded=sp.getBoolean("info",false)
        if(!isInfoReaded){
            btn_show_info.visibility=View.VISIBLE
            btn_show_info.setOnClickListener {
                sp.edit().putBoolean("info",true).apply()
                btn_show_info.visibility=View.GONE
                showInfoLaunch()
            }
        }

    }

    private fun showInfoLaunch() {
        val dialoge=Dialog(this)
        dialoge.setContentView(R.layout.first_launch_info)
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 50)
        dialoge.window?.setBackgroundDrawable(inset)
        dialoge.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialoge.setCancelable(false)
        val Pbtn=dialoge.findViewById<AppCompatButton>(R.id.close)
        Pbtn.setOnClickListener {
            dialoge.dismiss()
        }
        dialoge.show()
    }

    private fun getProductProtocol() {
        if(isNetworkConnected()){
            getProductsList(null,null,true)
        }else{
            if(firstLaunch){
                oneButtonDialog("أنت غير متصل بالأنترنت\uD83D\uDE13 ","يرجى التحقق من اتصالك بالشبكة","إعادة المحاولة",false,R.raw.nonet111,true)
            }else{oneButtonDialog("أنت غير متصل بالأنترنت\uD83D\uDE13 ","يرجى التحقق من اتصالك بالشبكة","حسناً",true,R.raw.nonet111,true)
            }
        }
    }

    private fun castToRecycler(list: ArrayList<productsData>) {
        val lm=LinearLayoutManager(this)
        recycler_products.layoutManager=lm
        recycler_products.adapter= ProductsAdapter(this,orderByPinned(list))
        hideLoad()
    }

    private fun dialogeCreatAccountOrLogin() {
        val dialogSheet = DialogSheet2(this)
            .setTitle("من فضلك قم بتسجيل الدخول أولاً ")
            .setMessage("يجب عليك إنشاء حساب أو تسجيل الدخول الى حسابك لكي تقوم بنشر الاعلانات")
            .setColoredNavigationBar(true)
            .setTitleTextSize(20) // In SP
            .setCancelable(true)
            .setNegativeButton("تسجيل الدخول") {
                closeMenu()
                startActivity(Intent(this,Login::class.java))

            }
            .setPositiveButton("إنشاء حساب") {
                closeMenu()
                startActivity(Intent(this,Signup::class.java))

            }


            .setRoundedCorners(true) // Default value is true
            .setBackgroundColor(this.resources.getColor(R.color.white)) // Your custom background color
            .setButtonsColorRes(R.color.red) // You can use dialogSheetAccent style attribute instead
            .setMessageTextColor(this.resources.getColor(R.color.black))
            .setNegativeButtonColor(this.resources.getColor(R.color.red))
            .setNegativeButtonColorRes(R.color.red)
            .show()
    }

    private fun adDialogeWarning() {
        val dialogSheet = DialogSheet2(this)
            .setTitle("عذراً")
            .setMessage("لايمكنك اضافة اكثر من 5 اعلانات في الوقت نفسه, يرجى حذف أحد الإعلانات ليمكنك من إضافة إعلان آخر")
            .setColoredNavigationBar(true)
            .setTitleTextSize(20) // In SP
            .setCancelable(true)
            .setNegativeButton("إلغاء") {

            }
            .setPositiveButton("إعلاناتي") {

                startActivity(Intent(this,MyAds::class.java))
//                finish()
            }
            .setRoundedCorners(true) // Default value is true
            .setBackgroundColor(this.resources.getColor(R.color.white)) // Your custom background color
            .setButtonsColorRes(R.color.red) // You can use dialogSheetAccent style attribute instead
            .setMessageTextColor(this.resources.getColor(R.color.black))
            .setNegativeButtonColor(this.resources.getColor(R.color.red))
            .setNegativeButtonColorRes(R.color.red)
            .show()
    }



    private fun openMenu() {
        val ani=AnimationUtils.loadAnimation(this,R.anim.rotate_right)
        btn_openMenu.startAnimation(ani).apply {
            val ani2=AnimationUtils.loadAnimation(applicationContext,R.anim.rotate_right)
            btn_openMenu.startAnimation(ani2).apply {
                btn_openMenu.setImageResource(R.drawable.ic_baseline_close_24)
                btn_openMenu.setBackgroundResource(R.drawable.temp18)
            }
        }
        cont_menu.visibility=View.VISIBLE
        val ani1=AnimationUtils.loadAnimation(this,R.anim.fab_slide_in_from_left)
        my_menu.startAnimation(ani1)

        if(edt_search.isFocused){
            closeKeyboard()
            edt_search.clearFocus()
        }
    }

    private fun closeMenu() {
        val ani=AnimationUtils.loadAnimation(this,R.anim.rotate_left)
        btn_openMenu.startAnimation(ani).apply {
            val ani2=AnimationUtils.loadAnimation(applicationContext,R.anim.rotate_left)
            btn_openMenu.startAnimation(ani2).apply {
                btn_openMenu.setImageResource(R.drawable.ic_baseline_table_rows_24)
                btn_openMenu.setBackgroundResource(R.drawable.temp17)
            }
        }
        val ani1=AnimationUtils.loadAnimation(this,R.anim.fab_slide_out_to_left)
        my_menu.startAnimation(ani1).apply {
                    cont_menu.visibility=View.GONE
        }
    }

    private fun convertToEnglish(s : CharSequence? ): CharSequence {
        var sAr=s
        sAr=sAr.toString().replace("٠","0")
        sAr=sAr.toString().replace("١","1")
        sAr=sAr.toString().replace("٢","2")
        sAr=sAr.toString().replace("٣","3")
        sAr=sAr.toString().replace("٤","4")
        sAr=sAr.toString().replace("٥","5")
        sAr=sAr.toString().replace("٦","6")
        sAr=sAr.toString().replace("٧","7")
        sAr=sAr.toString().replace("٨","8")
        sAr=sAr.toString().replace("٩","9")
        return sAr
    }
    private fun focusFix() {
        recycler_products.setOnScrollListener(object: OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (dropdown_city_products.isFocused){
                    dropdown_city_products.clearFocus()
                }
                if(edt_search.isFocused){
                    closeKeyboard()
                    edt_search.clearFocus()
                }

            }
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })
    }

    private fun filterFunc() {
        dropdown_city_products.setOnItemClickListener { adapterView, view, i, l ->
            citySelected=dropdown_city_products.text.toString()
            if(citySelected in resources.getStringArray(R.array.cityCustom)){
                if(citySelected=="الكل"){
                    cityFilterChecked=false
                    if(typeFilterChecked){
                        getProductsList(checkedId?.text.toString(),null,false)
                    }else{
                        getProductsList(null,null,false)
                    }
                }else{
                    cityFilterChecked=true
                    if(typeFilterChecked){
                        getProductsList(checkedId?.text.toString(),citySelected,false)
                    }else{
                        getProductsList(null,citySelected,false)
                    }
                }
            }
        }

        ct0.setOnClickListener{
            checkedTxtClicked(ct0)
        }
        ct1.setOnClickListener{
            checkedTxtClicked(ct1)
        }
        ct2.setOnClickListener{
            checkedTxtClicked(ct2)
        }
        ct3.setOnClickListener{
            checkedTxtClicked(ct3)
        }
        ct4.setOnClickListener{
            checkedTxtClicked(ct4)
        }

        ct6.setOnClickListener{
            checkedTxtClicked(ct6)

        }
        ct8.setOnClickListener{
            checkedTxtClicked(ct8)
        }
        ct9.setOnClickListener{
            checkedTxtClicked(ct9)
        }
        ct10.setOnClickListener{
            checkedTxtClicked(ct10)
        }
        ct11.setOnClickListener{
            checkedTxtClicked(ct11)
        }
        ct12.setOnClickListener{
            checkedTxtClicked(ct12)
        }
        ct13.setOnClickListener{
            checkedTxtClicked(ct13)
        }
        ct14.setOnClickListener{
            checkedTxtClicked(ct14)
        }
        ct15.setOnClickListener{
            checkedTxtClicked(ct15)
        }
        ct16.setOnClickListener{
            checkedTxtClicked(ct16)
        }
        ct17.setOnClickListener{
            checkedTxtClicked(ct17)
        }
    }

    private fun checkedTxtClicked(ctId: CheckedTextView) {
        if(isNetworkConnected()){

            if(!edt_search.text.isNullOrBlank()){edt_search.text.clear()}
            if(ct0.isChecked){ct0.isChecked=false}
            removeFilterEXCEPT(ctId.toString())
            ctId.isChecked=!ctId.isChecked
            if(cityFilterChecked && ctId.isChecked){
                typeFilterChecked=true
                checkedId=ctId
                getProductsList(ctId.text.toString(),citySelected,false)
            }else if(cityFilterChecked && (!ctId.isChecked)){
                ct0.isChecked=true
                checkedId=ct0
                typeFilterChecked=false
                getProductsList(null,citySelected,false)
            }else if((!cityFilterChecked) && ctId.isChecked){
                typeFilterChecked=true
                checkedId=ctId
                getProductsList(ctId.text.toString(),null,false)
            }else if((!cityFilterChecked) && (!ctId.isChecked)){
                ct0.isChecked=true
                checkedId=ct0
                typeFilterChecked=false
                getProductsList(null,null,false)
            }
        }else{
            oneButtonDialog("أنت غير متصل بالأنترنت\uD83D\uDE13 ","يرجى التحقق من اتصالك بالشبكة","حسناً",true,R.raw.nonet111,true)
        }
    }

    private fun removeFilterEXCEPT(id:String){
        if(ct1.toString() != id){ct1.isChecked=false}
        if(ct2.toString() != id){ct2.isChecked=false}
        if(ct3.toString() != id){ct3.isChecked=false}
        if(ct4.toString() != id){ct4.isChecked=false}
        if(ct6.toString() != id){ct6.isChecked=false}
        if(ct8.toString() != id){ct8.isChecked=false}
        if(ct9.toString() != id){ct9.isChecked=false}
        if(ct10.toString() != id){ct10.isChecked=false}
        if(ct11.toString() != id){ct11.isChecked=false}
        if(ct12.toString() != id){ct12.isChecked=false}
        if(ct13.toString() != id){ct13.isChecked=false}
        if(ct14.toString() != id){ct14.isChecked=false}
        if(ct15.toString() != id){ct15.isChecked=false}
        if(ct16.toString() != id){ct16.isChecked=false}

        checkedId=null
    }
    private fun fancyToastmaker(message:String, type: Int){
        FancyToast.makeText(applicationContext,message, FancyToast.LENGTH_LONG,type,false).show()
    }

    private fun getProductsList(type:String?=null,city:String?=null,getNewList:Boolean) {
        showLoad()
        mainProductList.clear()
        if(!getNewList){
            if(type != null){
                if(type != "الكل"){
                    for(i in allproductLisg){
                        if(i.type==type){
                            mainProductList.add(i)
                        }
                    }
                    firstLaunch=false
                }else{
                    for(y in allproductLisg){
                        mainProductList.add(y)
                    }
                }
            }else{
                for(y in allproductLisg){
                    mainProductList.add(y)
                }
            }
            if(city!=null){
                val working=ArrayList<productsData>()
                for(i in mainProductList){
                    if(i.city==city){
                        working.add(i)
                    }
                }
                mainProductList.clear()
                for(j in working){
                    mainProductList.add(j)
                }
            }
        }
        try{
            if(getNewList){
                mainProductList.clear()
                allproductLisg.clear()
                val query= ParseQuery.getQuery<ParseObject>("products")
                query.orderByDescending("updatedAt")
                query.findInBackground { objects, e ->
                    if(e==null){
                        if (objects != null) {
                            AsyncTask.execute {
                                for (i in objects) {
                                    val name = i.get("name").toString()
                                    val desc = i.get("desc").toString()
                                    val state = i.get("new_used").toString()
                                    val type1 = i.get("type").toString()
                                    val comm = i.get("comm").toString()
                                    val comm_tg=i.getBoolean("comm_tg")
                                    val comm_wa=i.getBoolean("comm_wa")
                                    val location = i.get("location").toString()
                                    val city1 = i.get("city").toString()
                                    val price = i.get("price").toString()
                                    val views = i.get("views").toString()
                                    val userName = i.get("username").toString()
                                    val Date = i.get("mdate").toString()
                                    var endDate = Date
                                    val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                                    val c = Calendar.getInstance()
                                    c.time = sdf.parse(endDate)
                                    c.add(Calendar.DATE, 10) // number of days to add
                                    endDate = sdf.format(c.time)
                                    var latesdVersion = ""
                                    var isForcedUpdate = false
                                    var url = ""
                                    val pending = i.getBoolean("pending")
                                    val objectId = i.objectId
                                    val pinned: Boolean = i.getBoolean("pinned")
                                    val image_1 = i.getParseFile("image1")
                                    val image_2 = i.getParseFile("image2")
                                    val image_3 = i.getParseFile("image3")
                                    val image_4 = i.getParseFile("image4")
                                    val bitmap = BitmapFactory.decodeStream(image_1!!.dataStream)
                                    var bitmap_2: Bitmap? = null
                                    if (image_2 != null) {
                                        bitmap_2 = BitmapFactory.decodeStream(image_2.dataStream)
                                    }
                                    var bitmap_3: Bitmap? = null
                                    if (image_3 != null) {
                                        bitmap_3 = BitmapFactory.decodeStream(image_3.dataStream)
                                    }
                                    var bitmap_4: Bitmap? = null
                                    if (image_4 != null) {
                                        bitmap_4 = BitmapFactory.decodeStream(image_4.dataStream)
                                    }
                                    val obj = productsData(
                                        name,
                                        bitmap,
                                        bitmap_2,
                                        bitmap_3,
                                        bitmap_4,
                                        desc,
                                        type1,
                                        state,
                                        city1,
                                        location,
                                        comm,
                                        comm_wa,
                                        comm_tg,
                                        pinned,
                                        price,
                                        objectId,
                                        views,
                                        userName,
                                        pending,
                                        endDate,
                                        Date
                                    )
                                    if (firstLaunch) {
                                        val sobj = searchProdData(objectId, name, desc, state)
                                        searchProductList.add(sobj)
                                        if (userName == "control") {
                                            isForcedUpdate = i.getBoolean("isForcedUpdate")
                                            latesdVersion = i.get("latestVersion").toString()
                                            url = i.get("updateHref").toString()
                                            if (latesdVersion != BuildConfig.VERSION_NAME) {
                                                newUpdateDiaoge(isForcedUpdate, url)
                                            }
                                        }

                                    }
                                    if (userName != "control") {
                                        mainProductList.add(obj)
                                        allproductLisg.add(obj)
                                    }
                                }
                                firstLaunch = false
                                runOnUiThread{
                                    mainProductList = orderByPinned(mainProductList)
                                    castToRecycler(mainProductList)

                                }
                            }


                        }
                    }else{saveError("null at get main products list")
//                        oneButtonDialog("تنبيه.!","عذراً,التطبيق قيد الصيانة حالياً.\nقد يستغرق حوالي يوم أو يومين\nشاكرين صبركم","خروج",false,R.raw.fix,false)
                        oneButtonDialog("أنت غير متصل بالأنترنت\uD83D\uDE13 ","يرجى التحقق من اتصالك بالشبكة","حسناً",true,R.raw.nonet111,true)

                    }
                }
            }else{
                mainProductList=orderByPinned(mainProductList)
                castToRecycler( mainProductList)
            }
        }catch (e:Exception){
            saveError(e.message.toString())
            oneButtonDialog("أنت غير متصل بالأنترنت\uD83D\uDE13 ","يرجى التحقق من اتصالك بالشبكة","حسناً",true,R.raw.nonet111,true)

//            oneButtonDialog("تنبيه.!","عذراً,التطبيق قيد الصيانة حالياً.\nقد يستغرق حوالي يوم أو يومين\nشاكرين صبركم","خروج",false,R.raw.fix,false)
//            finish()
        }


    }
    private fun orderByPinned(List: ArrayList<productsData>): ArrayList<productsData> {
        List.sortWith(object:Comparator<productsData>{
            override fun compare(p0: productsData?, p1: productsData?): Int {
                if (p1 != null) {
                    if (p0 != null) {
                        return p1.pinned.compareTo(p0.pinned)
                    }
                }
                return 0
            }
        })
        return List
    }

    private fun saveError(e:String) {
        val q=ParseObject("report")
        q.put("error",e)
        q.saveInBackground()
    }

    private fun oneButtonDialog(titleTxt:String,messageTxt:String,PbtnText:String,cancalable: Boolean,anim:Int,isInternet:Boolean=false){
        runOnUiThread {
        closeMenu()
        val dialoge=Dialog(this)
        dialoge.setContentView(R.layout.custom_dialog)
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 25)
        dialoge.window?.setBackgroundDrawable(inset)
        dialoge.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialoge.setCancelable(cancalable)
//        dialoge.window?.attributes?.windowAnimations=R.style.DialogAnimation
        val Pbtn=dialoge.findViewById<AppCompatButton>(R.id.dialog_btn_pos)
        val Nbtn=dialoge.findViewById<AppCompatButton>(R.id.dialoge_btn_neg)
        val Lanim=dialoge.findViewById<LottieAnimationView>(R.id.lottie_anim)
        val message=dialoge.findViewById<AutofitTextView>(R.id.dialog_message)
        val title=dialoge.findViewById<AutofitTextView>(R.id.dialog_title)

        title.text=titleTxt
        Pbtn.text=PbtnText
        Nbtn.visibility=View.GONE
        message.text=messageTxt
        Lanim.repeatMode= LottieDrawable.RESTART
        Lanim.setAnimation(anim)
        Pbtn.setOnClickListener {
            if(isInternet){
                if(!cancalable){
                    if(!isNetworkConnected()){
                        dialoge.dismiss()
                        oneButtonDialog(titleTxt,messageTxt,PbtnText,cancalable,anim,isInternet)
                    }else{
                        dialoge.dismiss()
                        getProductsList(null,null,true)
                    }
                }else{
                    dialoge.dismiss()
                }
            }else{
                if(PbtnText.equals("خروج")){
                    finishAffinity()
                }else{
                    dialoge.dismiss()
                }
            }
        }
        if(isInternet){
            (Lanim.layoutParams as ConstraintLayout.LayoutParams).dimensionRatio="H,1:1"
        }

        dialoge.show()
        }
    }



    private fun newUpdateDiaoge(forcedUpdate: Boolean,url:String) {
        runOnUiThread {
        closeMenu()
        val dialoge=Dialog(this)
        dialoge.setContentView(R.layout.custom_dialog)
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 25)
        dialoge.window?.setBackgroundDrawable(inset)
        dialoge.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialoge.setCancelable(!forcedUpdate)
        dialoge.window?.attributes?.windowAnimations=R.style.DialogAnimation
        val Pbtn=dialoge.findViewById<AppCompatButton>(R.id.dialog_btn_pos)
        val Nbtn=dialoge.findViewById<AppCompatButton>(R.id.dialoge_btn_neg)
        val Lanim=dialoge.findViewById<LottieAnimationView>(R.id.lottie_anim)
        val message=dialoge.findViewById<AutofitTextView>(R.id.dialog_message)
        Lanim.setAnimation(R.raw.test11111)


        if(forcedUpdate){
            message.text="من فضلك,لكي يعمل التطبيق بشكل صحيح يجب تحديثه الى اخر اصدار  "
            Nbtn.text="خروج \uD83D\uDE36\u200D\uD83C\uDF2B"
        }else{
            message.text="تم اضافة تحديثات جديدة وتحسينات الى السوق.\uD83D\uDE0E  \n للتمتع بالاضافات يرجى تنزيل اخر اصدار"
            Nbtn.text="الغاء \uD83D\uDE36\u200D\uD83C\uDF2B"
        }
        Pbtn.setOnClickListener {
            val uri=Uri.parse(url)
            val i=Intent(Intent.ACTION_VIEW)
            i.data = uri
            startActivity(i)

        }

        Nbtn.setOnClickListener {
            if(forcedUpdate){
                finishAffinity()
            }else{
                dialoge.dismiss()
            }
        }
        dialoge.show()

        }
    }


    override fun onRestart() {
        super.onRestart()
        if(needLocalRefreshing){
            castToRecycler(mainProductList)
        }
    }

    private fun isNetworkConnected(): Boolean {
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val isCon=cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isConnected
        return isCon
    }
    override fun onResume() {
        super.onResume()
        if(currentUser==null){
            btn_sec.text="تسجيل دخول"
            btn_third.text="إنشاء حساب"
        }else{
            btn_sec.text="حسابي"
            btn_third.text="تسجيل الخروج"
        }

    }

    private fun showFirstLaunch() {
        runOnUiThread {


        val dialoge=Dialog(this)
        dialoge.setContentView(R.layout.first_launch)
        val back=ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 25)
        val wlcom=dialoge.findViewById<TextView>(R.id.h1)
        val appName=dialoge.findViewById<TextView>(R.id.h11)

        val btn_creat=dialoge.findViewById<AppCompatButton>(R.id.btn_create)
        val btn_login=dialoge.findViewById<AppCompatButton>(R.id.btn_loggin)
        val btn_guest=dialoge.findViewById<AppCompatButton>(R.id.btn_guest)

        val btn_nxt=dialoge.findViewById<AppCompatImageButton>(R.id.nxttt)
        val firstDialoge=dialoge.findViewById<ConstraintLayout>(R.id.first_con)
        val secDialoge=dialoge.findViewById<ConstraintLayout>(R.id.con_options)
        val lottie=dialoge.findViewById<LottieAnimationView>(R.id.s1)
        dialoge.window?.setBackgroundDrawable(inset)
        dialoge.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialoge.setCancelable(false)
        val animOut=AnimationUtils.loadAnimation(this,R.anim.fab_scale_down)
        val animIn=AnimationUtils.loadAnimation(this,R.anim.fab_scale_up)
        val writer=dialoge.findViewById<TypeWriter>(R.id.j1)

        appName.visibility=View.INVISIBLE
        lottie.pauseAnimation()
        btn_nxt.visibility=View.INVISIBLE
        writer.visibility=View.INVISIBLE
        Thread{
            SystemClock.sleep(600)
            runOnUiThread {
                appName.startAnimation(AnimationUtils.loadAnimation(this,R.anim.smash)).apply {
                    appName.visibility=View.VISIBLE
                }
                Thread{
                    SystemClock.sleep(900)
                    runOnUiThread {
                        try{
                            writer.visibility=View.VISIBLE
                            writer.setCharacterDelay(40)
//                            val text="الطريقة الأبسط والأكثر فاعلية للشراء والبيع عبر الإنترنت"
                            val text="ملاحظة:\nهذه النسخة تجريبية وبنائأ على تجربة المستخدمين سيتم إطلاق النسخة النهائية\nوشكرآ."
                            writer.animateText(text)
                            var counter=0
                            for(i in text){counter+=1}
                            Thread {
                                SystemClock.sleep((counter*40).toLong()+300)
                                runOnUiThread {
                                    getProductProtocol()
                                    if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
                                        lottie.playAnimation()
                                    }
                                    btn_nxt.startAnimation(AnimationUtils.loadAnimation(this,android.R.anim.fade_in)).apply {
                                        btn_nxt.visibility=View.VISIBLE
                                    }
                                }
                            }.start()
                        }catch(e:Exception){}
                    }
                }.start()
            }
        }.start()
        btn_nxt.setOnClickListener {
            firstDialoge.startAnimation(animOut).apply {
                firstDialoge.visibility=View.GONE
                secDialoge.visibility=View.VISIBLE
                secDialoge.startAnimation(animIn).apply {
                    lottie.pauseAnimation()
                    btn_creat.setOnClickListener {
                        dialoge.dismiss()
                        startActivity(Intent(applicationContext,Signup::class.java))
                    }
                    btn_login.setOnClickListener {
                        dialoge.dismiss()
                        startActivity(Intent(applicationContext,Login::class.java))
                    }
                    btn_guest.setOnClickListener {
                        dialoge.dismiss()
                    }
                }
            }
        }
        dialoge.show()
        }
    }
    override fun onBackPressed() {
        if(cont_menu.isVisible){
            closeMenu()
        }else{
            if(numberOfReq==0){
                fancyToastmaker("اضغط مرة أخرى للخروج",FancyToast.INFO)
                numberOfReq+=1
            }else{
                this.finishAffinity()
            }
        }
    }

    private fun showLoad(){
        cont_lottie_loading_Prods.visibility=View.VISIBLE
        Log.d("3232","Showing Loading")
    }
    private fun closeKeyboard(){
        val view: View? = getCurrentFocus()
        if (view != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
    private fun hideLoad(){
        cont_lottie_loading_Prods.visibility=View.GONE
        recycler_products.visibility=View.VISIBLE
        Log.d("3232","Hiding Loading")

    }
}