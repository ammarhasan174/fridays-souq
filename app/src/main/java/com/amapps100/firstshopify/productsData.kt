package com.amapps100.firstshopify

import android.graphics.Bitmap
import java.io.File

data class productsData(
    var name : String, var image : Bitmap, var image_2 : Bitmap?, var image_3:Bitmap?
    , var image_4 : Bitmap?
    , var desc : String?
    , var type : String
    , var state:String
    , var city : String
    , var location :String
    , var comm : String
    ,var comm_wa:Boolean
    ,var comm_tg:Boolean
    , var pinned : Boolean=false
    , var price : String
    , var objectId:String
    , var views:String
    , var userName:String
    , var pending:Boolean
    , var endDate:String,
    var date:String?=null
)

// add city , location , communicate , email