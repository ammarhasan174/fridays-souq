package com.amapps100.firstshopify.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.net.ConnectivityManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.amapps100.firstshopify.*
import com.marcoscg.dialogsheet.DialogSheet2
import com.parse.ParseObject
import com.parse.ParseQuery
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MyAdsAdapter(val context: Activity, val myList:ArrayList<productsData>, var mListener: myAdsLoadingInterface) :
    RecyclerView.Adapter<MyAdsAdapter.ItemViewHolder>() {
    companion object{
        lateinit var customProd: productsData
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ItemViewHolder {
        val view : View= LayoutInflater.from(context).inflate(R.layout.products_layout,parent,false)
        return ItemViewHolder(view,mListener)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val currentProd=myList[position]
        holder.name.text = currentProd.name
        holder.desc.text = currentProd.desc
        holder.state.text = currentProd.state
        holder.city.text = currentProd.city
        holder.price.text=currentProd.price
        holder.date.text=currentProd.endDate
        holder.thisdate.text=currentProd.date
        if(currentProd.pinned){
            holder.cornerView.visibility=View.VISIBLE
            holder.star.visibility=View.VISIBLE
        }
        if(currentProd.pending){
            holder.pending.visibility=View.VISIBLE
        }

        holder.btn_delete.visibility=View.VISIBLE
        holder.image.setImageBitmap(currentProd.image)
        holder.image.background=
            BitmapDrawable(details().applyBlur(context,currentProd.image,20f))
        holder.menu_bottom.visibility=View.VISIBLE

        holder.itemView.setOnClickListener{
            ProductsAdapter.customProd = productsData(currentProd.name,currentProd.image
                ,currentProd.image_2,currentProd.image_3,currentProd.image_4
                ,currentProd.desc,currentProd.type,currentProd.state
                ,currentProd.city,currentProd.location,currentProd.comm,currentProd.comm_wa,currentProd.comm_tg,currentProd.pinned
                ,currentProd.price,currentProd.objectId,currentProd.views,currentProd.userName,currentProd.pending,currentProd.endDate,currentProd.date)
            context.startActivity(Intent(context, details::class.java))

            context.overridePendingTransition(R.anim.fade_in_fast,R.anim.fade_out_fast)
        }

        var endingSoon=false
        try{
            val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            val currentDate= Date()
            val cD=dateFormat.format(currentDate)
            var diff=dateFormat.parse(currentProd.endDate)!!.time - dateFormat.parse(cD)!!.time
            diff= (diff/(3.6e+6)).toLong()
            diff /= 24
            if(diff<=7){
                endingSoon=true
            }
        }catch (e: Exception){
        }

        holder.btn_openAdMenu.setOnClickListener{
            if(holder.btn_edit.isVisible){
                HideOptions(holder,endingSoon)
            }else{
                ShowOptions(holder,endingSoon)
            }
        }

        holder.btn_edit.setOnClickListener {
            HideOptions(holder,endingSoon)
            MyAds.editProd = productsData(currentProd.name,currentProd.image
                ,currentProd.image_2,currentProd.image_3,currentProd.image_4
                ,currentProd.desc,currentProd.type,currentProd.state
                ,currentProd.city,currentProd.location,currentProd.comm,currentProd.comm_wa,currentProd.comm_tg,currentProd.pinned
                ,currentProd.price,currentProd.objectId,currentProd.views,currentProd.userName,currentProd.pending,currentProd.endDate,currentProd.date)
            val i= Intent(context, MainActivity::class.java)
            i.putExtra("edit",true)
            context.startActivity(i)

        }

        holder.btn_pin.setOnClickListener {
            MyAds.message=Products.currentUser.toString()+":\n"+"خطة مدفوعة للإعلان "+currentProd.name
            mListener.showpay()
            HideOptions(holder,endingSoon)
        }

        holder.btn_delete.setOnClickListener {
            HideOptions(holder,endingSoon)
            if(isNetworkConnected(context)){
                val pos=position
                val dialogSheet = DialogSheet2(context)
                    .setTitle("هل أنت متأكد من حذف ${currentProd.name}")
                    .setMessage("")
                    .setColoredNavigationBar(true)
                    .setTitleTextSize(20) // In SP
                    .setCancelable(true)
                    .setIconBitmap(currentProd.image)
                    .setPositiveButton("حذف") {
                        mListener.showLoad()
                        toast(context,"جار الحذف...",FancyToast.INFO)
                        MyAds.canExit =false
                        val query= ParseQuery<ParseObject>("products")
                        query.whereEqualTo("username", Products.currentUser)
                        query.getInBackground(currentProd.objectId
                        ) { obj, e ->
                            if (e == null) {
                                obj?.deleteInBackground {
//                                    Products.numberOfAds-+1
                                    MyAds.canExit =true
                                    mListener.hideLoad()
                                    toast(context,"تم الحذف",FancyToast.SUCCESS)

                                    try{
                                        Products.mainProductList.remove(currentProd)
                                    }catch(e: Exception){}
                                    try{
                                        Products.allproductLisg.remove(currentProd)
                                    }catch(e: Exception){}
                                    myList.removeAt(pos)
                                    notifyItemRemoved(pos)
                                    Products.needLocalRefreshing =true
                                }
                                MyAds.canExit =true
                            } else {
                                MyAds.canExit =true
                                toast(context,"حدث خطأ ما \nيرجى المعاودة لاحقاً",FancyToast.ERROR)
                            }
                        }
                    }
                    .setNegativeButton("لا") {

                    }
                    .setRoundedCorners(true) // Default value is true
                    .setBackgroundColor(context.resources.getColor(R.color.white)) // Your custom background color
                    .setButtonsColorRes(R.color.red) // You can use dialogSheetAccent style attribute instead
                    .setMessageTextColor(context.resources.getColor(R.color.red))
                    .setNegativeButtonColor(context.resources.getColor(R.color.red))
                    .setNegativeButtonColorRes(R.color.red)
                    .show()
            }else{noInternetDialog(context)}

        }

        holder.btn_repost.setOnClickListener {
            HideOptions(holder,endingSoon)
            if(isNetworkConnected(context)){
                mListener.showLoad()
                val query= ParseQuery<ParseObject>("products")
                query.getInBackground(currentProd.objectId) { obj, e ->
                    if (e == null) {
                        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                        val date1 = Date()
                        var dateTime = dateFormat.format(date1)
                        obj.put("mdate",dateTime.toString())
                        obj.saveInBackground{e1->
                            if(e1==null){
                                toast(context,"تم تجديد الصلاحية",FancyToast.SUCCESS)

                                mListener.hideLoad()
                                try{
                                    Products.mainProductList.remove(currentProd)

                                }catch(e: Exception){}
                                try{
                                    Products.allproductLisg.remove(currentProd)

                                }catch(e: Exception){}
                                currentProd.date=dateTime
                                val c = Calendar.getInstance()
                                c.time = dateFormat.parse(dateTime)
                                c.add(Calendar.DATE, 15)
                                dateTime = dateFormat.format(c.time)
                                holder.date.text=dateTime
                                holder.thisdate.text=currentProd.date
                                currentProd.endDate=dateTime.toString()
                                try{
                                    Products.mainProductList.add(0,currentProd)
                                }catch(e: Exception){}
                                try{
                                    Products.allproductLisg.add(0,currentProd)
                                }catch(e: Exception){}
                                Products.needLocalRefreshing =true
                                notifyItemChanged(position)
                            }else{
                                toast(context,"حدث خطأ ما, يرجى المعاودة لاحقاً",FancyToast.ERROR)

                            }
                        }
                    }else{
                        toast(context,"حدث خطأ ما, يرجى المعاودة لاحقاً",FancyToast.ERROR)
                    }
                }
            }else{noInternetDialog(context)}
        }

    }

    private fun toast(c:Context,message:String,type:Int){
        FancyToast.makeText(c,message,FancyToast.LENGTH_SHORT,type,false).show()
    }

    override fun getItemCount(): Int {
        return myList.size
    }


    class ItemViewHolder(itemView: View, mListener: MyAdsAdapter.myAdsLoadingInterface) :RecyclerView.ViewHolder(itemView){
        val image=itemView.findViewById<ImageView>(R.id.imageView)
        val name=itemView.findViewById<TextView>(R.id.txt_pName)
        val desc=itemView.findViewById<TextView>(R.id.txt_pDesc)
        val state=itemView.findViewById<TextView>(R.id.txt_state)
        val city=itemView.findViewById<TextView>(R.id.txt_city)
        val price=itemView.findViewById<TextView>(R.id.txt_price)
        val pending=itemView.findViewById<TextView>(R.id.txt_pending)
        val date=itemView.findViewById<TextView>(R.id.date_recycler)
        val thisdate=itemView.findViewById<TextView>(R.id.thisDate)
        val btn_edit=itemView.findViewById<AppCompatButton>(R.id.btn_edit)
        val btn_pin=itemView.findViewById<AppCompatButton>(R.id.btn_pin)
        val btn_repost=itemView.findViewById<AppCompatButton>(R.id.btn_repost)
        val btn_delete=itemView.findViewById<ImageButton>(R.id.btn_delete)
        val cornerView=itemView.findViewById<View>(R.id.corner_view)
        val star=itemView.findViewById<ImageView>(R.id.view_star)
        val btn_openAdMenu=itemView.findViewById<AppCompatImageButton>(R.id.btn_openadmenu)
        val menu_bottom=itemView.findViewById<ConstraintLayout>(R.id.menu_bottom)
        val cont_date=itemView.findViewById<ConstraintLayout>(R.id.date_cont)
        val hideThem=itemView.findViewById<View>(R.id.view_hideThem)


//        val tabLayout=itemView.findViewById<TabLayout>(R.id.tabLayout)
    }


    private fun noInternetDialog(context: Context) {
        val dialogSheet = DialogSheet2(context)
            .setTitle("أنت غير متصل بالأنترنت\uD83D\uDE13 ")
            .setMessage("يرجى التحقق من اتصالك بالشبكة")
            .setColoredNavigationBar(true)
            .setTitleTextSize(20) // In SP
            .setCancelable(true)
            .setPositiveButton("موافق")
            .setRoundedCorners(false) // Default value is true
            .setBackgroundColor(context.resources.getColor(R.color.white)) // Your custom background color
            .setButtonsColorRes(R.color.red) // You can use dialogSheetAccent style attribute instead
            .setMessageTextColor(context.resources.getColor(R.color.black))
            .setTitleTextColor(context.resources.getColor(R.color.black))
            .show()
    }
    private fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(AppCompatActivity.CONNECTIVITY_SERVICE) as ConnectivityManager
        val isCon=cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isConnected
        return isCon
    }
    private fun HideOptions(holder: ItemViewHolder, endingSoon: Boolean) {
        if(holder.hideThem.isVisible){
            holder.hideThem.visibility=View.GONE
            val ani= AnimationUtils.loadAnimation(context, R.anim.fab_scale_down)
            holder.btn_edit.startAnimation(ani).apply{
                holder.btn_edit.visibility=View.INVISIBLE
            }
            holder.btn_pin.startAnimation(ani).apply {
                holder.btn_pin.visibility=View.INVISIBLE
            }
            if(endingSoon){
                holder.btn_repost.startAnimation(ani).apply {
                    holder.btn_repost.visibility=View.INVISIBLE
                }
                val ani1= AnimationUtils.loadAnimation(context, R.anim.fab_slide_out_to_right)
                holder.cont_date.startAnimation(ani1).apply{
                    holder.cont_date.visibility=View.INVISIBLE
                }
            }
        }

    }

    private fun ShowOptions(holder: ItemViewHolder, endingSoon: Boolean) {
        if(!holder.hideThem.isVisible){
            holder.hideThem.visibility=View.VISIBLE
            val ani= AnimationUtils.loadAnimation(context, R.anim.fab_scale_up)
            holder.btn_edit.startAnimation(ani).apply{
                holder.btn_edit.visibility=View.VISIBLE
            }
            holder.btn_pin.startAnimation(ani).apply {
                holder.btn_pin.visibility=View.VISIBLE
            }
            if(endingSoon){
                holder.btn_repost.startAnimation(ani).apply {
                    holder.btn_repost.visibility=View.VISIBLE
                }
                val ani1= AnimationUtils.loadAnimation(context, R.anim.fab_slide_in_from_left)
                holder.cont_date.startAnimation(ani1).apply{
                    holder.cont_date.visibility=View.VISIBLE
                }
            }
        }

    }

    interface myAdsLoadingInterface{
        fun showLoad()
        fun hideLoad()
        fun showpay()
    }





}