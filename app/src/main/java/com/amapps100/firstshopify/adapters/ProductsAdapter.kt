package com.amapps100.firstshopify.adapters

import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.amapps100.firstshopify.*
import com.amapps100.firstshopify.R
import com.parse.*
import kotlinx.android.synthetic.main.products_layout.view.*
import java.util.*
import kotlin.collections.ArrayList

class ProductsAdapter(val context: Activity, val myList:ArrayList<productsData>) :RecyclerView.Adapter<ProductsAdapter.ItemViewHolder>() {
    companion object{
        lateinit var customProd: productsData
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view : View= LayoutInflater.from(context).inflate(R.layout.products_layout,parent,false)

        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val currentProd=myList[position]
        holder.name.text = currentProd.name
        holder.desc.text = currentProd.desc
        holder.state.text = currentProd.state
        holder.city.text = currentProd.city
        holder.price.text=currentProd.price
        holder.date.text=currentProd.endDate
        holder.thisdate.text=currentProd.date
        if(currentProd.pinned){
            holder.cornerView.visibility=View.VISIBLE
            holder.star.visibility=View.VISIBLE
        }
        if(currentProd.pending){
            holder.itemView.visibility = View.GONE
            holder.itemView.layoutParams = RecyclerView.LayoutParams(0, 0)
        }
        holder.image.setImageBitmap(currentProd.image)
        holder.image.background=BitmapDrawable(details().applyBlur(context,currentProd.image,20f))
        holder.itemView.setOnClickListener{
            customProd = productsData(currentProd.name,currentProd.image
            ,currentProd.image_2,currentProd.image_3,currentProd.image_4
            ,currentProd.desc,currentProd.type,currentProd.state
            ,currentProd.city,currentProd.location,currentProd.comm,currentProd.comm_wa,currentProd.comm_tg,currentProd.pinned
            ,currentProd.price,currentProd.objectId,currentProd.views,currentProd.userName,currentProd.pending,currentProd.endDate,currentProd.date)
            val i=Intent(context, details::class.java)

            context.startActivity(i)
            context.overridePendingTransition(R.anim.fade_in_fast,R.anim.fade_out_fast)
        }

    }
    override fun getItemCount(): Int {
        return myList.size
    }


    class ItemViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView){
        val image=itemView.findViewById<ImageView>(R.id.imageView)
        val name=itemView.findViewById<TextView>(R.id.txt_pName)
        val desc=itemView.findViewById<TextView>(R.id.txt_pDesc)
        val state=itemView.findViewById<TextView>(R.id.txt_state)
        val city=itemView.findViewById<TextView>(R.id.txt_city)
        val price=itemView.findViewById<TextView>(R.id.txt_price)
        val date=itemView.findViewById<TextView>(R.id.date_recycler)
        val thisdate=itemView.findViewById<TextView>(R.id.thisDate)

        val cornerView=itemView.findViewById<View>(R.id.corner_view)
        val star=itemView.findViewById<ImageView>(R.id.view_star)



//        val tabLayout=itemView.findViewById<TabLayout>(R.id.tabLayout)
    }



}