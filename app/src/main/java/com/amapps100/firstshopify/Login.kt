package com.amapps100.firstshopify

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.marcoscg.dialogsheet.DialogSheet2
import com.parse.*
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btn_login_login.setOnClickListener {
            closeKeyboard()

            if(isNetworkConnected()){
                loginUser()
            }else{
                showNETdialog()
            }
        }
        btn_signup_login.setOnClickListener {
            startActivity(Intent(this,Signup::class.java))
        }

        btn_telegramLogin.setOnClickListener {
            val uri= Uri.parse("https://t.me/amapps100")
            val i=Intent(Intent.ACTION_VIEW)
            i.data = uri
            startActivity(i)
        }

//        btn_whatsappLogin.setOnClickListener {
//            val uri=Uri.parse("https://api.whatsapp.com/send?phone=PHONE_NUMBER")
//            val i=Intent(Intent.ACTION_VIEW)
//            i.data = uri
//            startActivity(i)
//        }
    }

    private fun loginUser() {
        if((!edt_UserName_login.text.isNullOrBlank()) && (!edt_pass_login.text.isNullOrBlank())){
            cont_lottie_loading_login.visibility= View.VISIBLE
            val userName=edt_UserName_login.text.toString().replace(" ","")
            val pass=edt_pass_login.text.toString().replace(" ","")
            ParseUser.logInInBackground(userName,
                pass){parseUser: ParseUser?, e: ParseException? ->
                cont_lottie_loading_login.visibility= View.GONE
                if(parseUser!=null){
                    Products.currentUser = userName
                    val sp=getSharedPreferences("db",MODE_PRIVATE)
                    sp.edit().putString("pass",pass).apply()
                    val ins=ParseInstallation.getCurrentInstallation()
                    ins.put("username",userName)
                    ins.saveInBackground(object:SaveCallback{
                        override fun done(e: ParseException?) {
                            if(e==null){finish()}else{fancyToastmaker(e.message.toString(),FancyToast.ERROR)}
                        }
                    })
                }else{
                    fancyToastmaker(e?.message.toString(),FancyToast.ERROR)
                }
            }
        }else{
            fancyToastmaker("يرجى ملئ جميع الحقول",FancyToast.WARNING)
        }
    }
    private fun closeKeyboard(){
        val view: View? = currentFocus
        if (view != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        if(window.currentFocus!=null){
            try{window.currentFocus!!.clearFocus()}catch (e:Exception){}
        }
    }
    private fun isNetworkConnected(): Boolean {
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val isCon=cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isConnected
        return isCon
    }

    private fun showNETdialog() {
        val dialogSheet = DialogSheet2(this)
            .setTitle("أنت غير متصل بالأنترنت\uD83D\uDE13 ")
            .setMessage("يرجى التحقق من اتصالك بالشبكة")
            .setColoredNavigationBar(true)
            .setTitleTextSize(20) // In SP
            .setCancelable(true)
            .setPositiveButton("موافق")
            .setRoundedCorners(false) // Default value is true
            .setBackgroundColor(this.resources.getColor(R.color.white)) // Your custom background color
            .setButtonsColorRes(R.color.red) // You can use dialogSheetAccent style attribute instead
            .setMessageTextColor(this.resources.getColor(R.color.black))
            .setTitleTextColor(this.resources.getColor(R.color.black))
            .show()
    }
    private fun fancyToastmaker(message:String, type: Int){
        FancyToast.makeText(applicationContext,message,FancyToast.LENGTH_SHORT,type,false).show()
    }
}