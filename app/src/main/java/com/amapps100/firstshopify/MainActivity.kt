package com.amapps100.firstshopify

import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.viewpager.widget.ViewPager
import com.marcoscg.dialogsheet.DialogSheet2
import com.parse.*
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.activity_main.*
import java.io.ByteArrayOutputStream
import java.lang.Exception
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {
    val REQUEST_CODE_STORAGE=101
    val REQUEST_CODE_SELECT_IMAGE=12
    val REQUEST_CODE_CROP_IMAGE=12412

    var currentImagePos=0

    var isEdit=false

    companion object{
        var afterCoppedBitmap:Bitmap?=null
        var imageArray = ArrayList<Bitmap>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageArray.clear()
        pagerView.adapter=CustomViewPager(this,imageArray,null)
        tabLayout_addProd.setupWithViewPager(pagerView,true)

        if(Products.currentUser!= null){
            txt_currentUserName.text=Products.currentUser
        }else{
            tempToastmaker(this,"يجب عليك تسجيل الدخول أولاً",FancyToast.INFO)
        }


        mainScrollView.setOnTouchListener(object:View.OnTouchListener{
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                txt_prodDesc.parent.requestDisallowInterceptTouchEvent(false)
                return false
            }
        })

        txt_prodDesc.setOnTouchListener(object:View.OnTouchListener{
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                txt_prodDesc.parent.requestDisallowInterceptTouchEvent(true)
                return false

            }
        })

        pagerViewTracker()
        focusFix()

        btn_canclefm.setOnClickListener {
            finish()
        }

        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        dropdown_newUsed.onFocusChangeListener = object:View.OnFocusChangeListener{
            override fun onFocusChange(p0: View?, p1: Boolean) {
                if(p1){
                    closeKeyboard()
                }
            }
        }
        dropdown_city.onFocusChangeListener = object:View.OnFocusChangeListener{
            override fun onFocusChange(p0: View?, p1: Boolean) {
                if(p1){
                    closeKeyboard()
                }
            }
        }
        dropdown_prodType.onFocusChangeListener = object:View.OnFocusChangeListener{
            override fun onFocusChange(p0: View?, p1: Boolean) {
                if(p1){
                    closeKeyboard()
                }
            }
        }

        btn_add_image.setOnClickListener {
            if(imageArray.size<=3){
                getImage_Permission()
            }else{
                tempToastmaker(this,"عذراً الحد الأقصى للصور هو 4 صور",FancyToast.CONFUSING)
                }
        }

        if(intent.extras!=null){
            if(intent.getBooleanExtra("edit",false)){
                isEdit=true
                txt_prodName.setText(MyAds.editProd.name)
                txt_prodDesc.setText(MyAds.editProd.desc)
                dropdown_prodType.setText(MyAds.editProd.type)
                dropdown_newUsed.setText(MyAds.editProd.state)
                dropdown_city.setText(MyAds.editProd.city)
                edt_comm.setText(MyAds.editProd.comm)
                edt_location.setText(MyAds.editProd.location)
                edt_price.setText(MyAds.editProd.price)
                chk_tele.isChecked=MyAds.editProd.comm_tg
                chk_whats.isChecked=MyAds.editProd.comm_wa
                val image_1=MyAds.editProd.image
                val image_2=MyAds.editProd.image_2
                val image_3=MyAds.editProd.image_3
                val image_4=MyAds.editProd.image_4
                imageArray.clear()
                imageArray.add(image_1)
                if(image_2 != null){
                    imageArray.add(image_2)
                }
                if(image_3 != null){
                    imageArray.add(image_3)
                }
                if(image_4 != null){
                    imageArray.add(image_4)
                    btn_add_image.visibility=View.GONE
                }
                pagerView.adapter=CustomViewPager(this,imageArray,null)
                tabLayout_addProd.setupWithViewPager(pagerView)
                real_pagerView_Cont.visibility= View.VISIBLE
                ta2.visibility=View.GONE
                btn_deleteImage.visibility=View.VISIBLE
                ta1.text="تعديل الإعلان"
            }
        }

        val productsType_newUsed=resources.getStringArray(R.array.new_used)
        val arrayAdapter_newUsed=ArrayAdapter(applicationContext,R.layout.dropdown_item,productsType_newUsed)
        dropdown_newUsed.setAdapter(arrayAdapter_newUsed)

        val productsCity=resources.getStringArray(R.array.city)
        val arrayAdapter_city=ArrayAdapter(applicationContext,R.layout.dropdown_item,productsCity)
        dropdown_city.setAdapter(arrayAdapter_city)

        val productsType=resources.getStringArray(R.array.productType)
        val arrayAdapter=ArrayAdapter(applicationContext,R.layout.dropdown_item,productsType)
        dropdown_prodType.setAdapter(arrayAdapter)

        btn_deleteImage.setOnClickListener{
            if(imageArray.size>0){
                if(imageArray.size==4){btn_add_image.visibility=View.VISIBLE}
                imageArray.removeAt(currentImagePos)
                pagerView.adapter=CustomViewPager(this,imageArray,null)
                pagerView.setCurrentItem(imageArray.lastIndex,true)
                if(imageArray.size==0){real_pagerView_Cont.visibility=View.GONE;
                    btn_deleteImage.visibility=View.GONE;
                    ta2.visibility=View.GONE}
            }

        }




        btn_uploaddata.setOnClickListener{
            if(
                (dropdown_prodType.text.toString() in resources.getStringArray(R.array.productType)) &&
                (dropdown_newUsed.text.toString() in resources.getStringArray(R.array.new_used)) &&
                (!txt_prodDesc.text.isNullOrBlank()) &&
                (!txt_prodName.text.isNullOrBlank())&&
                (!edt_comm.text.isNullOrBlank())&&
                (!edt_location.text.isNullOrBlank())&&
                ((dropdown_city.text.toString() in resources.getStringArray(R.array.city))) &&
                (!edt_price.text.isNullOrBlank())&&
                (imageArray.size>0))

                {
                    if(isNetworkConnected()){
                        showPlans()
                    }else{
                        noInternetDialog()
                    }
            }else{
                if(imageArray.size==0){
                    tempToastmaker(applicationContext,"الرجاء القيام بإضافة صورة واحدة على الأقل",FancyToast.WARNING)

                }
                if(txt_prodName.text.isNullOrBlank()){
                    txt_prodName.error="يرجى كتابة اسم الاعلان"
                    mainScrollView.smoothScrollTo(0,0)
                }
                if(txt_prodDesc.text.isNullOrBlank()){
                    txt_prodDesc.error="يرجى كتابة وصف عن المنتج"
                    mainScrollView.smoothScrollTo(0,0)
                }
                if(!(dropdown_prodType.text.toString() in resources.getStringArray(R.array.productType))){
                    dropdown_prodType.error="يرجى تحديد نوع المنتج"
                }
                if(!(dropdown_newUsed.text.toString() in resources.getStringArray(R.array.new_used))){
                    dropdown_newUsed.error="يرجى تحديد حالة المنتج"
                }
                if(!(dropdown_city.text.toString() in resources.getStringArray(R.array.city))){
                    dropdown_city.error="يرجى تحديد المدينة الخاصة بك"
                }
                if(edt_comm.text.isNullOrBlank()){
                    edt_comm.error="يرجى كتابة رقمك"
                }else{
                    val number=edt_comm.text.toString()
                    val regexStr = "^+[0-9]{10,13}$"
                    if(edt_comm.text.toString().length!=10 || !number.matches(regexStr.toRegex())){
                        edt_comm.error="يرجى ادخال رقم صالح"
                    }
                }
                if(edt_location.text.isNullOrBlank()){
                    edt_location.error="يرجى كتابة موقعك بشكل تقريبي"
                }
                if( edt_price.text.isNullOrBlank()){
                    edt_price.error="يرجى كتابة السعر"
                }
            }
        }
}

    private fun closeKeyboard() {
        val view: View? = currentFocus
        if (view != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun showPlans() {
        val dialoge=Dialog(this)
        dialoge.setContentView(R.layout.ad_options)
        val back=ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 25)
        val mlll1=dialoge.findViewById<RelativeLayout>(R.id.lll1)
        val mlll2=dialoge.findViewById<RelativeLayout>(R.id.lll2)
        val txtFree=dialoge.findViewById<ConstraintLayout>(R.id.q2)
        val txtPaid=dialoge.findViewById<ConstraintLayout>(R.id.d3)
//        val btn_next=dialoge.findViewById<AppCompatButton>(R.id.btn_next)
        dialoge.window?.setBackgroundDrawable(inset)
        dialoge.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialoge.setCancelable(true)

        try{
            MyAds.message=Products.currentUser.toString()+":\n"+"خطة مدفوعة للإعلان "+txt_prodName.text.toString()
        }catch (e:Exception){}
        txtFree.setOnClickListener{
            mlll1.performClick()
        }
        txtPaid.setOnClickListener{
            mlll2.performClick()
        }
        mlll1.setOnClickListener {
            dialoge.dismiss()
            if(isEdit){
                updateData()
            }else{
                uploadData()
            }
            MyAds.showPaymentIns=false
            try{
                mlll2.backgroundTintList=null
                mlll1.backgroundTintList= AppCompatResources.getColorStateList(this,R.color.color_selected)
            }catch (e:Exception){}
        }
        mlll2.setOnClickListener {
            dialoge.dismiss()
            if(isEdit){
                updateData()
            }else{
                uploadData()
            }
            MyAds.showPaymentIns=true
            try{
                mlll1.backgroundTintList=null
                mlll2.backgroundTintList= AppCompatResources.getColorStateList(this,R.color.color_selected)
            }catch (e:Exception){}
        }

        dialoge.show()
    }



    private fun showLoading() {
        cont_lottie_loading_main.visibility=View.VISIBLE

    }

    private fun closeLoading(){
        cont_lottie_loading_main.visibility=View.GONE
    }


    private fun convertToEnglish(s : CharSequence? ): CharSequence {
        var sAr=s
        sAr=sAr.toString().replace("٠","0")
        sAr=sAr.toString().replace("١","1")
        sAr=sAr.toString().replace("٢","2")
        sAr=sAr.toString().replace("٣","3")
        sAr=sAr.toString().replace("٤","4")
        sAr=sAr.toString().replace("٥","5")
        sAr=sAr.toString().replace("٦","6")
        sAr=sAr.toString().replace("٧","7")
        sAr=sAr.toString().replace("٨","8")
        sAr=sAr.toString().replace("٩","9")
        return sAr
    }



    private fun updateData() {
        val query=ParseQuery<ParseObject>("products")
        query.whereEqualTo("username",Products.currentUser)
        showLoading()
        query.getInBackground(MyAds.editProd.objectId
        ) { obj, e ->
            if (e == null) {
                obj?.updatedAt
                val name = txt_prodName.text.toString()
                obj?.put("name", convertToEnglish(name))
                obj?.put("desc", convertToEnglish(txt_prodDesc.text.toString()))
                obj?.put("type", dropdown_prodType.text.toString())
                obj?.put("new_used", dropdown_newUsed.text.toString())
                obj?.put("location",convertToEnglish(edt_location.text.toString()))
                obj?.put("city", dropdown_city.text.toString())
                obj?.put("comm", convertToEnglish(edt_comm.text.toString().replace(" ","")))
                obj?.put("comm_wa",chk_whats.isChecked)
                obj?.put("comm_tg",chk_tele.isChecked)
                obj?.put("username", Products.currentUser!!)
                obj?.put("pending", true)
                obj?.put("reviewed", false)
                val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                val date1 = Date()
                var dateTime = dateFormat.format(date1)
                obj?.put("mdate",dateTime.toString())
                var price=convertToEnglish(edt_price.text.toString()).toString()
                try {
                    val formatter: NumberFormat = DecimalFormat("#,###")
                    val formattedNumber = formatter.format(price.toInt())
                    price = formattedNumber
                } catch (e: Exception) {
                }
                obj?.put("price", price)
                var j=0
                for (i in 0 until imageArray.size) {
                    if(j<4){
                        j+=1
                        val imageBytes: ByteArray =
                            Base64.decode(convertImagetoString(imageArray[i]), Base64.DEFAULT)
                        val image = ParseFile("${imageArray.size}.jpeg", imageBytes)
                        obj?.put("image${i + 1}", image)
                    }
                }

                obj.saveInBackground(object : SaveCallback {
                    override fun done(e: ParseException?) {
                        if (e == null) {
                            Products.mainProductList.remove(MyAds.editProd)
                            Products.allproductLisg.remove(MyAds.editProd)
                            MyAds.editProd.name=convertToEnglish(name).toString()
                            MyAds.editProd.desc=convertToEnglish(txt_prodDesc.text.toString()).toString()
                            MyAds.editProd.type=dropdown_prodType.text.toString()
                            MyAds.editProd.state=dropdown_newUsed.text.toString()
                            MyAds.editProd.location=convertToEnglish(edt_location.text.toString()).toString()
                            MyAds.editProd.city=dropdown_city.text.toString()
                            MyAds.editProd.comm=convertToEnglish(edt_comm.text.toString()).toString()
                            MyAds.editProd.userName=Products.currentUser!!
                            MyAds.editProd.pending=true
                            MyAds.editProd.date=dateTime.toString()
                            MyAds.editProd.comm_tg=chk_tele.isChecked
                            MyAds.editProd.comm_wa=chk_whats.isChecked
                            val c = Calendar.getInstance()
                            c.time = dateFormat.parse(dateTime)
                            c.add(Calendar.DATE, 15)
                            dateTime = dateFormat.format(c.time)
                            MyAds.editProd.endDate=dateTime.toString()
                            MyAds.editProd.price=price
                            for(i in 1..imageArray.size){
                                when(i){
                                    1->{MyAds.editProd.image= imageArray[0]}
                                    2->{MyAds.editProd.image_2= imageArray[1]}
                                    3->{MyAds.editProd.image_3= imageArray[2]}
                                    4->{MyAds.editProd.image_4= imageArray[3]}
                                }
                            }
                            Products.mainProductList.add(MyAds.editProd)
                            Products.allproductLisg.add(MyAds.editProd)
                            Products.needLocalRefreshing=true
                            MyAds.needMyAdsREfresh=true
                            MyAds.myAds_PeningInfo=true
                            closeLoading()
                            finish()

                        } else {
                            closeLoading()
                            tempToastmaker(this@MainActivity, e.message.toString(),FancyToast.ERROR)
                        }
                    }
                })

            } else {
                closeLoading()
                tempToastmaker(this@MainActivity,e.message.toString(),FancyToast.ERROR)
                tempToastmaker(
                    this@MainActivity,
                    "المنتج غير موجود \nقد تم حذفه او ازالته لتجاوزه المدة المعينة للنشر",FancyToast.ERROR
                )
            }
        }
    }

    private fun focusFix() {
        mainScrollView.viewTreeObserver.addOnScrollChangedListener{
//            if(dropdown_city.isFocused){
//                dropdown_city.clearFocus()
//            }
//            if(dropdown_newUsed.isFocused){
//                dropdown_newUsed.clearFocus()
//            }
//            if(dropdown_prodType.isFocused){
//                dropdown_prodType.clearFocus()
//            }
        }
    }

    private fun pagerViewTracker() {
        pagerView.addOnPageChangeListener(object:ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                currentImagePos=position
            }
            override fun onPageSelected(position: Int) {

            }
            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }

    private fun uploadData(){
        val parser=ParseObject("products")
        val name=txt_prodName.text.toString()
        parser.put("name",convertToEnglish(name))
        parser.put("desc",convertToEnglish(txt_prodDesc.text.toString()))
        parser.put("type",dropdown_prodType.text.toString())
        parser.put("new_used",dropdown_newUsed.text.toString())
        parser.put("location",convertToEnglish(edt_location.text.toString()))
        parser.put("city",dropdown_city.text.toString())
        parser.put("comm",convertToEnglish(edt_comm.text.toString().replace(" ","")))
        parser.put("comm_wa",chk_whats.isChecked)
        parser.put("comm_tg",chk_tele.isChecked)
        parser.put("username",Products.currentUser!!)
        parser.put("pending",true)
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val date1 = Date()
        val dateTime = dateFormat.format(date1)
        parser.put("mdate",dateTime)
        var price=convertToEnglish(edt_price.text.toString()).toString()
        try {
            val formatter: NumberFormat = DecimalFormat("#,###")
            val formattedNumber = formatter.format(price.toInt())
            price = formattedNumber
        } catch (e: Exception) {
        }
        parser.put("price",price)
        var j=0
        for(i in 0 until imageArray.size){
            if(j<4){
                j+=1
                val imageBytes: ByteArray = Base64.decode(convertImagetoString(imageArray[i]), Base64.DEFAULT)
                val image=ParseFile("${imageArray.size}.jpeg",imageBytes)
                parser.put("image${i+1}",image)
            }
        }
        try{
            showLoading()
            parser.saveInBackground{
                if(it != null){
                    tempToastmaker(applicationContext,it.message.toString(),FancyToast.ERROR)

                    closeLoading()
                }else{
                    closeLoading()
                    MyAds.newItemAdded=true
                    Products.needLocalRefreshing=true
                    MyAds.needMyAdsREfresh=true
                    MyAds.myAds_PeningInfo=true
                    finish()
                    startActivity(Intent(applicationContext,MyAds::class.java))
                }
            }

        }catch (e:Exception){
            closeLoading()
            tempToastmaker(applicationContext,e.message.toString(),FancyToast.ERROR)

        }
    }
    private fun getImage_Permission(){
        if(ContextCompat.checkSelfPermission(this,android.Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),REQUEST_CODE_STORAGE)
        }else{
            selectImage()
        }
    }

    private fun tempToastmaker(context:Context,message:String,type:Int){
        FancyToast.makeText(context,message,FancyToast.LENGTH_SHORT,type,false).show()
    }

    private fun isNetworkConnected(): Boolean {
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val isCon=cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isConnected
        return isCon
    }
    private fun noInternetDialog() {
        val dialogSheet = DialogSheet2(this)
            .setTitle("أنت غير متصل بالأنترنت\uD83D\uDE13 ")
            .setMessage("يرجى التحقق من اتصالك بالشبكة")
            .setColoredNavigationBar(true)
            .setTitleTextSize(20) // In SP
            .setCancelable(true)
            .setPositiveButton("موافق")
            .setRoundedCorners(false) // Default value is true
            .setBackgroundColor(this.resources.getColor(R.color.white)) // Your custom background color
            .setButtonsColorRes(R.color.red) // You can use dialogSheetAccent style attribute instead
            .setMessageTextColor(this.resources.getColor(R.color.black))
            .setTitleTextColor(this.resources.getColor(R.color.black))
            .show()
    }
    fun reduceBitmapSize(bitmap: Bitmap, MAX_SIZE: Int): Bitmap {
        val ratioSquare: Double
        val bitmapHeight: Int = bitmap.height
        val bitmapWidth: Int = bitmap.width
        ratioSquare = (bitmapHeight * bitmapWidth / MAX_SIZE).toDouble()
        if (ratioSquare <= 1) return bitmap
        val ratio = Math.sqrt(ratioSquare)
        Log.d("mylog", "Ratio: $ratio")
        val requiredHeight = Math.round(bitmapHeight / ratio).toInt()
        val requiredWidth = Math.round(bitmapWidth / ratio).toInt()
        return Bitmap.createScaledBitmap(bitmap, requiredWidth, requiredHeight, true)
    }


    private fun convertImagetoString(bm:Bitmap): String? {
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) // bm is the bitmap object
        val b: ByteArray = baos.toByteArray()
        val encodedImage = Base64.encodeToString(b, Base64.DEFAULT)
        return encodedImage
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == REQUEST_CODE_STORAGE && grantResults.isNotEmpty()){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                selectImage()
            }else{
                tempToastmaker(this,"الرجاء إعطاء إذن الوصول إلى الصور لإتمام العملية",FancyToast.WARNING)
                }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_CODE_SELECT_IMAGE && resultCode == RESULT_OK){
            if(data != null ){
                val selectedImageUri=data.data
                if(selectedImageUri != null){
                    try{
                        val inputStream=contentResolver.openInputStream(selectedImageUri)
                        val bitmap= BitmapFactory.decodeStream(inputStream)
                        val resizedBitMap=reduceBitmapSize(bitmap,250000)
                        cropImage(resizedBitMap)

                    }catch (e: Exception){
                        tempToastmaker(applicationContext,e.message.toString(),FancyToast.ERROR)
                    }
                }
            }
        }else if(requestCode==REQUEST_CODE_CROP_IMAGE){
            if(resultCode==RESULT_OK){
                if(imageArray.size<=4 && (afterCoppedBitmap!=null)){
                    imageArray.add(afterCoppedBitmap!!)
                    if(!real_pagerView_Cont.isVisible){real_pagerView_Cont.visibility=View.VISIBLE;
                        btn_deleteImage.visibility=View.VISIBLE;
                        ta2.visibility=View.VISIBLE
                    }
                }
                if(imageArray.size==4){
                    btn_add_image.visibility=View.GONE
                }else{
                    if(!btn_add_image.isVisible){
                        btn_add_image.visibility=View.GONE
                    }
                }
                pagerView.adapter=CustomViewPager(this@MainActivity,imageArray,null)
                pagerView.setCurrentItem(imageArray.lastIndex,true)
            }
        }
    }
    private fun selectImage(){
        val intent= Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        if(intent.resolveActivity(packageManager) != null){
            startActivityForResult(intent,REQUEST_CODE_SELECT_IMAGE)
        }
    }

    private fun cropImage(imageBitmap: Bitmap) {
        val i= Intent(this, CropImage::class.java)
        val bStream = ByteArrayOutputStream()
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bStream)
        val byteArray: ByteArray = bStream.toByteArray()
        i.putExtra("image", byteArray)
        if(intent.resolveActivity(packageManager) != null){
            startActivityForResult(i,REQUEST_CODE_CROP_IMAGE)
        }
    }

    override fun onBackPressed() {
        if(!cont_lottie_loading_main.isVisible){
            super.onBackPressed()
            finish()
        }else{
            tempToastmaker(this,"الرجاء الأنتظار قليلا..",FancyToast.INFO)
        }
    }
}