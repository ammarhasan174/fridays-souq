package com.amapps100.firstshopify

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class CustomViewPager(val context: Context, val imageList: ArrayList<Bitmap>,var mListener:ViewPagerInterface?) : PagerAdapter() {
    // on below line we are creating a method
    // as get count to return the size of the list.
    override fun getCount(): Int {
        return imageList.size
    }

    // on below line we are returning the object
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as ConstraintLayout
    }

    // on below line we are initializing
    // our item and inflating our layout file
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        // on below line we are initializing
        // our layout inflater.
        val mLayoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        // on below line we are inflating our custom
        // layout file which we have created.
        val itemView: View = mLayoutInflater.inflate(R.layout.items, container, false)

        // on below line we are initializing
        // our image view with the id.
        val imageView: ImageView = itemView.findViewById<View>(R.id.imageViewMain) as ImageView

        // on below line we are setting
        // image resource for image view.
        imageView.setImageBitmap(imageList[position])
        // on the below line we are adding this

        // on below line we are simply
        // returning our item view.
        Objects.requireNonNull(container).addView(itemView)
        if(mListener!=null){
            itemView.setOnClickListener {
                mListener!!.fullScreenFuns()
            }
        }
        return itemView
    }

    // on below line we are creating a destroy item method.
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        // on below line we are removing view
        container.removeView(`object` as ConstraintLayout)
    }

    interface ViewPagerInterface{
        fun fullScreenFuns()
    }
}