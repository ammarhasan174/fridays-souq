package com.amapps100.firstshopify


import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.Toast
import com.application.isradeleon.notify.Notify
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.parse.fcm.ParseFCM
import org.json.JSONObject
import java.lang.Exception

class NotyReciver : FirebaseMessagingService() {
    override fun onMessageReceived(message: RemoteMessage) {
        Log.d("131","asdasd")
        try{
            var alert=""
            var title=getString(R.string.app_name)
            var href:String?=null
            var objId:String?=null
            try{
                val jsonSTR=message.data["data"].toString()
                val jsonObj= JSONObject(jsonSTR)
                try{alert=jsonObj.getString("alert")}catch (e:Exception){Log.d("332",e.message.toString())}
                try{title=jsonObj.getString("title")}catch (e:Exception){Log.d("332",e.message.toString())}
                try{href=jsonObj.getString("href")}catch (e:Exception){Log.d("332",e.message.toString())}
                try{objId=jsonObj.getString("objectId")}catch (e:Exception){Log.d("332",e.message.toString())}
            }catch(e: Exception){
            }
//        notify
            var i=Intent(this,Products::class.java)
            if(href!=null){
                i=openHref(href)
            }else if(objId!=null){
                val i_1=Intent(this,details::class.java)
                i_1.putExtra("objectId",objId)
                i=i_1
            }
            Notify.build(this)
                .setId(684590)
                .setTitle(title)
                .setContent(alert)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setColor(R.color.white)
                .setImportance(Notify.NotifyImportance.HIGH)
                .setAction(i)
                .show();
        }catch (e: Exception){
        }
    }

    private fun openHref(href:String):Intent{
        val uri= Uri.parse(href)
        val i=Intent(Intent.ACTION_VIEW)
        i.data = uri
        return i
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        ParseFCM.register(token)
    }
}