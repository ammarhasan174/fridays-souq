package com.amapps100.firstshopify

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import com.marcoscg.dialogsheet.DialogSheet2
import com.parse.*
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.android.synthetic.main.activity_signup.*
import java.util.ArrayList
import kotlin.Exception
import android.widget.Toast

import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import kotlinx.android.synthetic.main.activity_login.*


class Signup : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        btn_signup_signup.setOnClickListener {
            closeKeyboard()
            if(isNetworkConnected()){
            var muid="null"
            try{
                muid= Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID).toString()
            }catch (e:Exception){}
            if(muid!="null"){
                val query=ParseQuery(ParseUser.getQuery())
                query.whereEqualTo("uid",muid)
                query.findInBackground { objects, e ->
                    var count = 0
                    val names= arrayListOf<String>()
                    if (objects != null) {
                        for (i in objects) {
                            count++
                            names.add(i.username)
                        }
                    }
                    if (count >= 2) {
                        showDialoge(names)
                    } else {
                        signUpNewUser()
                    }
                }
            }else{
                signUpNewUser()
            }
            }else{
                showNETdialog()
            }
        }
        btn_telegramSignup.setOnClickListener {
            val uri= Uri.parse("https://t.me/amapps100")
            val i=Intent(Intent.ACTION_VIEW)
            i.data = uri
            startActivity(i)
        }

//        btn_whatsappSignup.setOnClickListener {
//            val uri=Uri.parse("https://api.whatsapp.com/send?phone=PHONE_NUMBER")
//            val i=Intent(Intent.ACTION_VIEW)
//            i.data = uri
//            startActivity(i)
//        }

    }
    private fun closeKeyboard(){
        val view: View? = currentFocus
        if (view != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        if(window.currentFocus!=null){
            try{window.currentFocus!!.clearFocus()}catch (e:Exception){}
        }
    }
    private fun showDialoge(names: ArrayList<String>) {
        val dialogSheet = DialogSheet2(this)
            .setTitle("عذراً,")
            .setMessage(" لايمكنك انشاء اكثر من حسابين على نفس الجهاز\n1.${names[0]} \n2.${names[1]}")
            .setColoredNavigationBar(true)
            .setTitleTextSize(20) // In SP
            .setCancelable(true)
            .setNegativeButton("الغاء") {

            }
            .setPositiveButton("تواصل مع الدعم") {
                val uri= Uri.parse("https://t.me/amapps100")
                startActivity(Intent(Intent.ACTION_VIEW,uri))
            }


            .setRoundedCorners(true) // Default value is true
            .setBackgroundColor(this.resources.getColor(R.color.white)) // Your custom background color
            .setButtonsColorRes(R.color.red) // You can use dialogSheetAccent style attribute instead
            .setMessageTextColor(this.resources.getColor(R.color.black))
            .setNegativeButtonColor(this.resources.getColor(R.color.red))
            .setNegativeButtonColorRes(R.color.red)
            .show()
    }

    private fun signUpNewUser() {
        if((!edt_UserName_signup.text.isNullOrBlank())
            &&(!edt_pass_signup.text.isNullOrBlank())
            && (!edt_confirmpass_signup.text.isNullOrBlank())
            &&(!edt_UserName_signup.text.toString().contains(" "))
            &&(!edt_pass_signup.text.toString().contains(" "))){
            val userName=edt_UserName_signup.text.toString()
            val pass1=edt_pass_signup.text.toString()
            val pass2=edt_confirmpass_signup.text.toString()
            if(pass1 != pass2){
                edt_confirmpass_signup.error="تأكد كن تطابق كلمتي السر"
                edt_confirmpass_signup.requestFocus()
            }else{
                if(pass1.length < 6){
                    edt_pass_signup.error="لايمكن ان تكون كلمة السر أقل من 6 محارف"
                    edt_pass_signup.requestFocus()
                }else{
                    cont_lottie_loading_sign.visibility= View.VISIBLE
                    val parseUser=ParseUser()
                    parseUser.username=userName
                    parseUser.setPassword(pass1)
                    var uid="null"
                    try{
                        uid= Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID).toString()
                    }catch (e:Exception){}
                    parseUser.put("uid",uid)
                    parseUser.signUpInBackground {
                        cont_lottie_loading_sign.visibility= View.GONE
                        if (it == null) {
                            Products.currentUser = userName
                            val sp=getSharedPreferences("db",MODE_PRIVATE)
                            sp.edit().putString("pass",pass1).apply()
                            fancyToastmaker("تم انشاء الحساب بنجاح", FancyToast.SUCCESS)
                            val ins=ParseInstallation.getCurrentInstallation()
                            ins.put("username",userName)
                            ins.saveInBackground(object:SaveCallback{
                                override fun done(e: ParseException?) {
                                    if(e==null){finish()}else{fancyToastmaker(e.message.toString(), FancyToast.ERROR)}
                                }
                            })

                        } else {
                            fancyToastmaker(it.message.toString(), FancyToast.ERROR)
                        }
                    }
                }
            }
        }else{
            if(edt_UserName_signup.text.toString().contains(" ")){
                edt_UserName_signup.error="لايجب ان يحتوي اسم المستخدم على مسافات"
                edt_UserName_signup.requestFocus()
            }else if(edt_pass_signup.text.toString().contains(" ")){
                edt_pass_signup.error="لايجب ان تحتوي كلمة السر على مسافات"
                edt_pass_signup.requestFocus()
            }else{
                fancyToastmaker("يرجى ملئ جميع الحقول",FancyToast.WARNING)
            }
        }
    }
    private fun isNetworkConnected(): Boolean {
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val isCon=cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isConnected
        return isCon
    }

    private fun showNETdialog() {
        val dialogSheet = DialogSheet2(this)
            .setTitle("أنت غير متصل بالأنترنت\uD83D\uDE13 ")
            .setMessage("يرجى التحقق من اتصالك بالشبكة")
            .setColoredNavigationBar(true)
            .setTitleTextSize(20) // In SP
            .setCancelable(true)
            .setPositiveButton("موافق")
            .setRoundedCorners(false) // Default value is true
            .setBackgroundColor(this.resources.getColor(R.color.white)) // Your custom background color
            .setButtonsColorRes(R.color.red) // You can use dialogSheetAccent style attribute instead
            .setMessageTextColor(this.resources.getColor(R.color.black))
            .setTitleTextColor(this.resources.getColor(R.color.black))
            .show()
    }
    private fun fancyToastmaker(message:String, type: Int){
        FancyToast.makeText(applicationContext,message,FancyToast.LENGTH_SHORT,type,false).show()
    }
}