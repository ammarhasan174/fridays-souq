package com.amapps100.firstshopify

import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.net.ConnectivityManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.lottie.LottieAnimationView
import com.airbnb.lottie.LottieDrawable
import com.amapps100.firstshopify.adapters.MyAdsAdapter
import com.marcoscg.dialogsheet.DialogSheet2
import com.parse.*
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.android.synthetic.main.activity_my_ads.*
import me.grantland.widget.AutofitTextView
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MyAds : AppCompatActivity(), MyAdsAdapter.myAdsLoadingInterface   {

    private var myAds=ArrayList<productsData>()

    companion object{
        lateinit var editProd:productsData
        var canExit=true
        var needMyAdsREfresh=false
        var newItemAdded=false
        var myAds_PeningInfo=false
        var showPaymentIns=false
        var message="مرحباً,\nأريد الخطة المدفوعة لإعلاني"
        var lastUpdatedList=ArrayList<productsData>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_ads)
        if(myAds_PeningInfo){
            myAds_PeningInfo=false
            dialog_Pending()
        }

        currentUser.text=Products.currentUser
        val sp=getSharedPreferences("db", MODE_PRIVATE)
        currentUserPass.text=sp.getString("pass","Error")

        if(isNetworkConnected()){
            if (newItemAdded){
                newItemAdded=false
                getNewList(Products.currentUser!!)
            }else if(lastUpdatedList.size>0){
                castToRecyceler(lastUpdatedList)
            }else{
                getMyAds(Products.currentUser!!)
            }
        }else{
            showNETdialog()
        }
        if(showPaymentIns){
            showPaymentIns=false
            paymentDialog()
        }
    }

    private fun showadv(){
        val dialoge=Dialog(this)
        dialoge.setContentView(R.layout.info_dialog)
        val back=ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 25)
        val btn_nxt=dialoge.findViewById<AppCompatButton>(R.id.nxt)
        dialoge.window?.setBackgroundDrawable(inset)
        dialoge.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialoge.setCancelable(true)
        btn_nxt.setOnClickListener {
            dialoge.dismiss()
            paymentDialog()
        }
        dialoge.show()
    }

    private fun paymentDialog(){
        val dialoge=Dialog(this)
        dialoge.setContentView(R.layout.complete_pay)
        val back=ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 25)
        val btn_copy=dialoge.findViewById<AppCompatButton>(R.id.btn_copy)
        val btn_what=dialoge.findViewById<AppCompatButton>(R.id.btn_viaWhats)
        val btn_teleg=dialoge.findViewById<AppCompatButton>(R.id.btn_viaTeleg)
        val mMessage=dialoge.findViewById<TextView>(R.id.mMessage)
        mMessage.text= message
        dialoge.window?.setBackgroundDrawable(inset)
        dialoge.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialoge.setCancelable(true)
        btn_copy.setOnClickListener {
            val clipboard: ClipboardManager =getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("message", mMessage.text.toString())
            clipboard.setPrimaryClip(clip)
            FancyToast.makeText(this,"تم نسخ النص",FancyToast.LENGTH_SHORT,FancyToast.SUCCESS,false).show()
        }
//        btn_what.setOnClickListener {
//            val uri=Uri.parse("https://api.whatsapp.com/send?phone=PHONE_NUMBER&text=${mMessage.text.toString()}")
//            val i=Intent(Intent.ACTION_VIEW)
//            i.data = uri
//            dialoge.dismiss()
//            startActivity(i)
//        }

        btn_teleg.setOnClickListener {
            val uri= Uri.parse("https://t.me/amapps100")
            val i= Intent(Intent.ACTION_VIEW)
            i.data = uri
            dialoge.dismiss()
            startActivity(i)
        }

        dialoge.show()
    }

    private fun dialog_Pending(){
        val titleTxt="تم حفظ إعلانك بنجاح \uD83E\uDD1D "
        val messageTxt="لأسباب تتعلق بالحفاظ على جودة السوق\nسيتم تعليق الإعلان حتى تتم مراجعته من قبلنا.\nلن تستغرق مدة التعليق أكثر من 6 ساعات"
        val PbtnText="حسناً"
        val dialoge= Dialog(this)
        dialoge.setContentView(R.layout.custom_dialog)
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 25)
        dialoge.window?.setBackgroundDrawable(inset)
        dialoge.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialoge.setCancelable(true)
        val Pbtn=dialoge.findViewById<AppCompatButton>(R.id.dialog_btn_pos)
        val Nbtn=dialoge.findViewById<AppCompatButton>(R.id.dialoge_btn_neg)
        val Lanim=dialoge.findViewById<LottieAnimationView>(R.id.lottie_anim)
        val message=dialoge.findViewById<AutofitTextView>(R.id.dialog_message)
        val title=dialoge.findViewById<AutofitTextView>(R.id.dialog_title)
        title.text=titleTxt
        Pbtn.text=PbtnText
        Nbtn.visibility=View.GONE
        message.text=messageTxt
        Lanim.repeatMode= LottieDrawable.RESTART
        Lanim.setAnimation(R.raw.pendingpurple)
        Lanim.speed=0.7f
        (Lanim.layoutParams as ConstraintLayout.LayoutParams).dimensionRatio="H,1:1"
        Pbtn.setOnClickListener {
            dialoge.dismiss()
        }
        dialoge.show()
    }


    private fun showNETdialog() {
        val dialogSheet = DialogSheet2(this)
            .setTitle("أنت غير متصل بالأنترنت\uD83D\uDE13 ")
            .setMessage("يرجى التحقق من اتصالك بالشبكة")
            .setColoredNavigationBar(true)
            .setTitleTextSize(20) // In SP
            .setCancelable(true)
            .setPositiveButton("موافق")
            .setRoundedCorners(false) // Default value is true
            .setBackgroundColor(this.resources.getColor(R.color.white)) // Your custom background color
            .setButtonsColorRes(R.color.red) // You can use dialogSheetAccent style attribute instead
            .setMessageTextColor(this.resources.getColor(R.color.black))
            .setTitleTextColor(this.resources.getColor(R.color.black))
            .show()
    }

    private fun getNewList(currentUser: String) {
        showLoad()
        lastUpdatedList.clear()
        val query= ParseQuery.getQuery<ParseObject>("products")
        query.addDescendingOrder("updatedAt")
        query.whereEqualTo("username",currentUser)
        query.findInBackground { objects, e ->
            if(e==null){
                if (objects != null) {
                    for (i in objects) {
                        val name = i.get("name").toString()
                        val desc = i.get("desc").toString()
                        val state = i.get("new_used").toString()
                        val type1 = i.get("type").toString()
                        val comm = i.get("comm").toString()
                        val comm_tg=i.getBoolean("comm_tg")
                        val comm_wa=i.getBoolean("comm_wa")
                        val location = i.get("location").toString()
                        val city1 = i.get("city").toString()
                        val price = i.get("price").toString()
                        val views = i.get("views").toString()
                        val userName = i.get("username").toString()
                        val Date = i.get("mdate").toString()
                        var endDate = Date
                        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                        val c = Calendar.getInstance()
                        c.time = sdf.parse(endDate)
                        c.add(Calendar.DATE, 10) // number of days to add
                        endDate = sdf.format(c.time)
                        val pending = i.getBoolean("pending")
                        val objectId=i.objectId
                        val pinned : Boolean=i.getBoolean("pinned")
                        val image_1 = i.getParseFile("image1")
                        val image_2 = i.getParseFile("image2")
                        val image_3 = i.getParseFile("image3")
                        val image_4 = i.getParseFile("image4")
                        val bitmap = BitmapFactory.decodeStream(image_1!!.dataStream)
                        var bitmap_2 : Bitmap?=null
                        if(image_2 != null){
                            bitmap_2 = BitmapFactory.decodeStream(image_2.dataStream)
                        }
                        var bitmap_3 : Bitmap?=null
                        if(image_3 != null){
                            bitmap_3 = BitmapFactory.decodeStream(image_3.dataStream)
                        }
                        var bitmap_4 : Bitmap?=null
                        if(image_4 != null){
                            bitmap_4 = BitmapFactory.decodeStream(image_4.dataStream)
                        }
                        val obj = productsData(name, bitmap,bitmap_2,bitmap_3,bitmap_4, desc,type1,state,city1,location,comm,comm_wa,comm_tg,pinned,price,objectId,views,userName,pending,endDate,Date)

                        if(userName!="control"){
                            lastUpdatedList.add(obj)
                        }
                    }
                    castToRecyceler(lastUpdatedList)
                }
            }else{finishAffinity()}
        }
    }

    private fun getMyAds(username: String){
        myAds.clear()
        showLoad()
        if(lastUpdatedList.size==0){
            for(i in Products.allproductLisg){
                if(i.userName==username){
                    myAds.add(i)
                }
            }
        }else{
            myAds=lastUpdatedList
        }
        hideLoad()
        if(myAds.isEmpty()){
            txt_noAds.visibility= View.VISIBLE
        }else{
            castToRecyceler(myAds)
        }
    }

    private fun orderByPinned(List: ArrayList<productsData>): ArrayList<productsData> {
        List.sortWith(object:Comparator<productsData>{
            override fun compare(p0: productsData?, p1: productsData?): Int {
                if (p1 != null) {
                    if (p0 != null) {
                        return p1.pinned.compareTo(p0.pinned)
                    }
                }
                return 0
            }
        })
        return List
    }
    private fun castToRecyceler(list:ArrayList<productsData>){
        val lm= LinearLayoutManager(this)
        recycler_myAds.layoutManager=lm
        recycler_myAds.adapter= MyAdsAdapter(this,orderByPinned(list),this)
        hideLoad()
    }
    override fun onBackPressed() {
        if(canExit){
            super.onBackPressed()
        }else{

            FancyToast.makeText(this,"الرجاء الأنتظار ريثما يتم الحذف",FancyToast.LENGTH_SHORT,FancyToast.INFO,false).show()
        }
    }
    override fun onRestart() {
        super.onRestart()
        if (needMyAdsREfresh){
            needMyAdsREfresh=false
            getMyAds(Products.currentUser!!)
        }
        if(myAds_PeningInfo){
            myAds_PeningInfo=false
            dialog_Pending()
        }
        if(showPaymentIns){
            showPaymentIns=false
            paymentDialog()
        }
    }

    private fun isNetworkConnected(): Boolean {
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val isCon=cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isConnected
        return isCon
    }

    override fun showLoad() {
        cont_lottie_loading_ads.visibility=View.VISIBLE
    }

    override fun hideLoad() {
        cont_lottie_loading_ads.visibility=View.GONE
    }

    override fun showpay() {
        showadv()
    }

}