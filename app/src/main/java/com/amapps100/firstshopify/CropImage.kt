package com.amapps100.firstshopify

import android.content.Context
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.view.WindowManager
import com.canhub.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_crop_image.*
import android.view.MotionEvent
import java.lang.Exception


class CropImage : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_crop_image)

        if(intent!=null){
            val bArray=intent.getByteArrayExtra("image")
            val bmp= BitmapFactory.decodeByteArray(bArray,0,bArray!!.size)
            croppedImageView.setAspectRatio(4,3)
            croppedImageView.setImageBitmap(bmp)
            MainActivity.afterCoppedBitmap=bmp



            backback_crop.setImageBitmap(applyBlur(this,bmp,25f))
            croppedImageView.setOnSetCropOverlayReleasedListener{
                btn_saveImage.isEnabled=true
                MainActivity.afterCoppedBitmap=croppedImageView.croppedImage
            }
            croppedImageView.cornerShape=CropImageView.CropCornerShape.OVAL
            btn_rotateImage.setOnClickListener {
                croppedImageView.rotateImage(90)
            }
            btn_saveImage.setOnClickListener {
                setResult(RESULT_OK)
                finish()
            }
            btn_cancle.setOnClickListener {
                setResult(RESULT_CANCELED)
                finish()
            }
        }else{
            setResult(RESULT_CANCELED)
            finish()
        }


    }

    fun applyBlur(pContext: Context?, pSrcBitmap: Bitmap?, pBlurRadius: Float): Bitmap? {
        return if (pSrcBitmap != null) {
            val copyBitmap = pSrcBitmap.copy(Bitmap.Config.ARGB_8888, true)
            val outputBitmap = Bitmap.createBitmap(copyBitmap)
            val renderScript = RenderScript.create(pContext)
            val scriptIntrinsicBlur =
                ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript))
            val allocationIn = Allocation.createFromBitmap(renderScript, pSrcBitmap)
            val allocationOut = Allocation.createFromBitmap(renderScript, outputBitmap)
            scriptIntrinsicBlur.setRadius(pBlurRadius)
            scriptIntrinsicBlur.setInput(allocationIn)
            scriptIntrinsicBlur.forEach(allocationOut)
            allocationOut.copyTo(outputBitmap)
            outputBitmap
        } else {
            null
        }
    }
}